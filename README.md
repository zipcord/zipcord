# Zipcord

[Documentation](https://zipcord.gitlab.io/zipcord/) |
[NPM](https://www.npmjs.com/package/zipcord) |
[Source](https://gitlab.com/zipcord/zipcord) |
[Demo site](https://zipcord.gitlab.io/zipcord-demo/) |
[Benchmarks](https://zipcord.gitlab.io/zipcord-demo/bench.html)


## Features

* Uses [HTTP Range][] requests to download only the data you need
* One external dependency in default configuration*
* ZIP64 support
* Extract files to buffers or streams

<sup>*Depends on [zipcord-decoder-fflate], which is a small wrapper around
    [fflate][]. This can be reduced to zero dependencies by importing
    [`ZipcordRaw`][] and employing tree shaking, however you will need to
    supply your own decoder.</sup>

[HTTP Range]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range
[zipcord-decoder-fflate]: https://gitlab.com/zipcord/zipcord-decoder-fflate

## Install

```npm install zipcord```

Or your package manager's equivalent command. After installing it will be
available as a package named `zipcord`.


## Usage

```typescript
import { Zipcord } from from "zipcord";
const archive = await Zipcord.open("https://example.com/archive.zip");
// Print array of all filenames contained in the archive
console.log(archive.filenames);
const entry = archive.entries.get("example.txt");
// archive.entries is a Map and entry would be undefined if "sample.txt" was
// not the name of an archive entry, actual code should check the result before
// using it

// Extracting as a Uint8Array
const buffer = await entry.buffer();
console.log("Length of buffer in bytes: ", buffer.byteLength);
// Extracting as text using a specified encoding
const text = await entry.text("ascii");
console.log("File entry as ASCII text: ", text);
// Extracting as a ReadableStream
const stream = await entry.bufferStream();
stream.pipeTo(myWritableStream);
```

It's recommended to open archives using [`Zipcord.open`][]. If you want to
instantiate the object yourself you must call [`initialize`][] afterwards:

```typescript
const archive = new Zipcord(myZipcordOptions);
await archive.initialize("http://example.com/archive.zip");
```

See [`Zipcord`](https://zipcord.gitlab.io/zipcord/classes/zipcord.html)'s
documentation for the full API.

[`Zipcord.open`]: https://zipcord.gitlab.io/zipcord/classes/zipcord.html#open
[`initialize`]: https://zipcord.gitlab.io/zipcord/classes/zipcord.html#initialize

### Decoders

Zipcord allows the built-in Deflate decoder to be overridden with an
alternative implementation. This is done by setting [`decoderSetup`][] on the
options (example using the WASM-based decoder [`zipcord-decoder-flate2`][]):

[`decoderSetup`]: https://zipcord.gitlab.io/zipcord/interfaces/zipcordoptions.html#decodersetup
[`zipcord-decoder-flate2`]: https://gitlab.com/zipcord/zipcord-decoder-flate2

```typescript
import { Zipcord } from from "zipcord";
import { createDecoderConfig } from "zipcord-decoder-flate2";
const options = { decoderSetup: createDecoderConfig() }
const archive = await Zipcord.open("http://example.com/archive.zip", options)
// Compressed archive entries will now be extracted using zipcord-decoder-flate2
```

`decoderSetup` is set to a function that returns a `Promise` that resolves to
a decoder configuration. If this setup function fails (for example, a .wasm
module could not be loaded) Zipcord will fallback to the internal decoder. You
can use use [`ZipcordRaw`][] instead of `Zipcord` if you do not want this
behavior.

See documentation on
[`DecoderConfig`](https://zipcord.gitlab.io/zipcord/interfaces/decoderconfig.html)
for information on implementing new decoders.

### Node.js

Zipcord was designed for browsers but works with Node.js as well. When running
under Node Zipcord uses native zlib to decode compressed entries rather than
the Javascript-based decoder. It also provides the additional methods
[`bufferStreamNode`][] and [`textStreamNode`][] on `ZipEntry` to allow
extracting to [Node Readable streams][].

[`bufferStreamNode`]: https://zipcord.gitlab.io/zipcord/classes/zipentry.html#bufferstreamnode
[`textStreamNode`]: https://zipcord.gitlab.io/zipcord/classes/zipentry.html#textstreamnode
[Node Readable streams]: https://nodejs.org/api/stream.html#stream_readable_streams


## Compatibility notes

Zipcord should work with most ZIP archives, but there are a few features it
does not support.

* Compression methods other than Deflate or store
* The proprietary Deflate64 method is also not supported
* Encrypted files of any type
* Multiple-part archives
* Creating or editing archives

While Zipcord theoretically allows 4GB+ archives to be opened, implementation
limits in current browsers may cause problems with large files. If you need to
read very large archives you may have better luck reading them as streams.

Additionally, while ZIP64 is supported, due to limitations in Javascript the
maximum size of an archive or decompressed file entry is approximately eight
petabytes (2<sup>53</sup> - 1 bytes). This is unlikely to be an issue.

### Platform compatibility

Zipcord should work in any reasonably modern browser. Older browsers may
require polyfills for features such as
[`AbortController`](https://caniuse.com/abortcontroller). Zipcord can also be
used with Node.js, however currently if you want to read archives over HTTP it
requires polyfills for `fetch` and `AbortController`.


## Alternatives

Zipcord's primary goal is to allow reading individual archive entries over
HTTP without downloading the entire archive. If you have other requirements
such as minimizing bundle size or creating archives other libraries may be more
suitable for your use. Some popular alternatives are compared below:

|                | [JSZip][] | [UZIP.js][] | [fflate][] | Zipcord |
| ----------------------------------- | :--: | :--: | :--: | :--: |
| Extract to stream                   |  ✓   |  ✗   |  ✗   |  ✓   |
| ES6 modules                         |  ✗   |  ✗   |  ✓   |  ✓   |
| Create/edit archives                |  ✓   |  ✓   |  ✓   |  ✗   |
| HTTP Range reading                  |  ✗   |  ✗   |  ✗   |  ✓   |
| Pluggable decoder                   |  ✗   |  ✗   |  ✗   |  ✓   |
| Typescript types                    |  ✓   |  ✗   |  ✓   |  ✓   |
| Bundle size (minified estimate)     | 93kb | 14kb | 5kb* | 48kb |

<sup>*After tree shaking, assuming only ZIP archive decompression is used</sup>

[UZIP.js]: https://github.com/photopea/UZIP.js


## Build instructions

Zipcord requires at least Typescript 4.0. After cloning the repository and
installing devDependancies with `npm install`, it may be built with `npm build`.
Transpiled output is written to `./dist`. Useful NPM scripts include:

* `build` - Transpile source to `./dist`
* `build:docs` - Build documentation to `./docs` using Typedoc
* `clean` - Removes built output
* `lint` - Check source with ESLint
* `test` - Run unit tests
* `util:dump` - Dumps information about an archive to the terminal. Run with
    path or URL as the only argument.


## Credits/license
Zipcord is copyright © David Powell and is licenced under MIT. See LICENSE.txt
for the full text of the license.

Test files located in src/testdat/files are from [JSZip][] and are licenced
under MIT.

ZIP format specification
[APPNOTE.TXT](https://support.pkware.com/home/pkzip/developer-tools/appnote)
was created and is maintained by PKWARE.


[fflate]: https://github.com/101arrowz/fflate
[JSZip]: https://stuk.github.io/jszip/
[`ZipcordRaw`]: https://zipcord.gitlab.io/zipcord/classes/zipcordraw.html
