import nodeResolve from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'


export default [
  // CommonJS bundle for Node.js require() imports. Dependencies are not
  // bundled and Zipcord is ZipcordNode.
  {
    input: 'dist/index.js',
    external: ['fflate', 'stream', 'zipcord-decoder-fflate', 'zlib'],
    output: [
      {
        format: 'cjs',
        file: 'dist/zipcord.commonjs.cjs',
        exports: 'named',
      },
    ],
  },
  // UMD bundle for Unpkg. Dependencies are bundled and Zipcord is ZipcordFflate.
  {
    input: 'dist/index.js',
    output: [
      {
        format: 'umd',
        file: 'dist/zipcord.umd.js',
        exports: 'named',
        name: 'Zipcord',
        compact: true,
        plugins: [
          // This build is intended to be served from a CDN, minimize size
          terser({
            compress: {
              passes: 2,
              unsafe: true
            }
          }),
        ]
      },
    ],
    plugins: [
      // Resolve using browser-specific modules
      nodeResolve({
        browser: true,
      }),
    ]
  }
]
