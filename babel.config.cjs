// Used by Jest to transform ES6 code.

module.exports = {
  presets: ['@babel/preset-env']
}
