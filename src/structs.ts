/** Structures and utility methods relating to them for zip files.

Note that Zip64 8-byte values are stored as regular numbers. The maximum
integer that can be stored in a `Number` is 2**53 - 1, or about 8 petabytes.
This is not a bug and will not be fixed, using `BigInt`s to store these values
introduces complexity, compabitility, and performance issues, just to provide
the ability to read files that might not actually even exist anywhere in real
life. If you find yourself needing to work with zip files larger than this I
suggest not using Javascript to do this, and also running far, far away from
whatever clusterfuck led to your current situation.

@packageDocumentation
*/

import { ZipInvalidError, ZipRangeError, ZipUnsupportedError } from './errors.js'
import { decodeBufferSlice } from './util.js'


// Loosen naming in the enums
/* eslint-disable @typescript-eslint/naming-convention */

/** Potential values for compressionMethod. Almost always Store or Deflate. */
export enum CompressionMethod {
  Store        = 0,
  Shrink       = 1,
  Reduce_1     = 2,
  Reduce_2     = 3,
  Reduce_3     = 4,
  Reduce_4     = 5,
  Implode      = 6,
  Tokenize     = 7,
  Deflate      = 8,
  Deflate64    = 9,
  IBM_Terse    = 10,
  // Reserved   (11)
  BZip2        = 12,
  // Reserved   (13)
  LZMA         = 14,
  // Reserved   (15)
  IBM_CMPSC    = 16,
  // Reserved   (17)
  IBM_TerseNew = 18,
  IBM_LZ77_z   = 19,
  JPEG         = 96,
  WavPack      = 97,
  PPMd         = 98,
  AEx          = 99
}

/** IDs of known possible extraField IDs */
export enum ExtraFieldIds {
  AV                    = 0x0007,
  Encryption            = 0x0017,
  FileStream            = 0x000e,
  IBMS390               = 0x0065,
  IBMS390Compressed     = 0x0066,
  NTFS                  = 0x000a,
  OpenVMS               = 0x000c,
  OS2                   = 0x0009,
  Patch                 = 0x000f,
  PFSExtended           = 0x0008,
  PKCS7Recipients       = 0x0019,
  PKCS7Store            = 0x0014,
  Policy                = 0x0021,
  POSZIP4690            = 0x4690,
  Record                = 0x0018,
  SMartcryptPolicy      = 0x0023,
  SmartcryptProvider    = 0x0022,
  Timestamp             = 0x0020,
  Unix                  = 0x000d,
  X509CD                = 0x0016,
  X509File              = 0x0015,
  Zip64                 = 0x0001,
  // Known third party IDs from Zip specs
  Acorn                 = 0x4341,
  AEx                   = 0x9901,
  AOSVSACL              = 0x5356,
  ASi                   = 0x756e,
  BeOS                  = 0x6542,
  ExtendedTimestamp     = 0x5455,
  FWKCSMD5              = 0x4b46,
  InfoZIPComment        = 0x6375,
  InfoZIPMacintosh      = 0x334d,
  InfoZipOpenVMS        = 0x4d49,
  InfoZIPPath           = 0x7075,
  InfoZIPUNIX           = 0x5855,
  InfoZIPUNIXNew        = 0x7855,
  Macintosh             = 0x07c8,
  MicrosoftOPGH         = 0xa220,
  MVS                   = 0x470f,
  OS2ACL                = 0x4c41,
  SMS                   = 0xfd4a,
  VMCMS                 = 0x4704,
  WinNTACL              = 0x4453,
  XceedLocation         = 0x4f4c,
  XceedUnicode          = 0x554e,
  ZipItMacintosh        = 0x2605,
  ZipItMacintosh135File = 0x2705,
  ZipItMacintosh135Dir  = 0x2805,
}

/** Potential values of the host system field of versionMadeBy */
export enum VersionHost {
  DOS       = 0,
  Amiga     = 1,
  OpenVMS   = 2,
  UNIX      = 3,
  VM_CMS    = 4,
  AtariST   = 5,
  OS2_HPFS  = 6,
  Macintosh = 7,
  ZSystem   = 8,
  CP_M      = 9,
  NTFS      = 10,
  MVS       = 11,
  VSE       = 12,
  AcornRisc = 13,
  VFAT      = 14,
  MVS_Alt   = 15,
  BeOS      = 16,
  Tandem    = 17,
  OS_400    = 18,
  OSX       = 19
}

/* eslint-enable @typescript-eslint/naming-convention */

/** Use in DataView calls to read data in little endian order */
export const LITTLE_ENDIAN = true
/** Used in reading 64-bit values, equalivant to 2097151 */
const MAX_SAFE_INT_HIGH_DWORD = 0b00011111_11111111_11111111
/** All zip comment length fields are 2 bytes */
export const ZIP_COMMENT_MAX_SIZE = 65_535
/** A field with this value means the actual number is stored in a Zip64 structure */
export const ZIP64_U32_PLACEHOLDER = 0xffffffff

/** Info-ZIP Extra Field (0x7075) */
export interface ExtraFieldInfozipUnicode {
  /** Version of this structure (currently 1 is the only version) */
  version: number
  /** CRC32 of the containing structure's filename. Unicode name should not
   * be used if the CRC does not match the entry's. */
  entryNameCRC32: number
  /** Full unicode name for file entry */
  filename: string
}

/** Zip64 Extended Information Extra Field (0x0001).

  Note that while the size and offset fields are 2^64 bits by spec, a JS
  `Number` only supports integers up to 2^53 bits in size, putting an upper
  limit on these fields of 8PB.
*/
export interface ExtraFieldZip64 {
  /** Size of entry when uncompressed */
  uncompressedSize: number
  /** Size of compressed data */
  compressedSize?: number
  /** Offset of the local header */
  localHeaderOffset?: number
  /** Disk number of start of this file entry */
  diskNumberStart?: number
}

/** Object mapping a numeric field ID to its raw buffer data. Certain fields
  used by Zipcord are parsed and available as properties of the object.
*/
export interface ZipExtraFieldMapping {
  /** Info-ZIP Unicode path field, if present */
  infozipUnicode?: ExtraFieldInfozipUnicode
  /** Zip64 Extended Information Extra Field, if present */
  zip64?: ExtraFieldZip64
  /** All fields by numeric ID */
  [extraFieldId: number]: ArrayBuffer
}

/** VersionMadeBy substructure */
export interface ZipVersionMadeBy {
  /** Host platform for file attributes */
  host: VersionHost
  /** Zip specification supported by creator of file. Stored as a two-digit
   * decimal value, e.g. 6.2.0 is encoded as 62. */
  specification: number
  /** Raw (unparsed) value of this field */
  _raw: number
}

/** General purpose bit flag bitfield */
export interface ZipGeneralFlags {
  /** File entry is encrypted */
  encrypted: boolean
  /** Flags specific to the file entry's compression method */
  compressionSettings: number
  /** If set CRC-32 and file size fields follow compressed data */
  descriptorFollows: boolean
  /** File entry is compressed patched data */
  patched: boolean
  /** File entry uses strong encryption */
  strongEncryption: boolean
  /** Filename and comment fields must be UTF-8 if set */
  languageEncoding: boolean
  /** Fields in local header are masked when strong encryption used */
  masked: boolean
  /** Raw (unparsed) value of flags field */
  _raw: number
}

export interface ZipEOCD {
  /** Should be 0x06054b50 */
  signature: number      // 4 bytes
  /** Number of this disk */
  diskNum: number        // 2 bytes
  /** Number of the disk with the start of the central directory */
  diskWithCD: number     // 2 bytes
  /** Total number of entries in the central directory on this disk */
  entriesOnDisk: number  // 2 bytes
  /** Total number of entries in the central directory */
  entriesTotal: number   // 2 bytes
  /** Size of the central directory */
  sizeofCD: number       // 4 bytes
  /** Offset of start of central directory with respect to the starting disk number */
  offsetCD: number       // 4 bytes
  /** .ZIP file comment length */
  commentLength: number  // 2 bytes
  /** .ZIP file comment */
  comment: string        // Value of commentLength
  /** Location of EOCD in file */
  _offset: number
}
/** Minimum size of EOCD. Actual size is larger if the file has a comment. */
export const ZIP_EOCD_MIN_SIZE = 22
export const ZIP_EOCD_SIGNATURE = 0x06054b50

/** Zip64 end of central directory locator */
export interface Zip64EOCDLocator {
  /** Should be 0x07064b50 */
  signature: number           // 4 bytes
  /** Number of the disk with the start of the zip64 end of central directory */
  diskWithZip64EOCD: number   // 4 bytes
  /** Relative offset of the zip64 end of central directory record */
  offsetZip64EOCD: number     // 8 bytes
  /** Total number of disks */
  diskTotal: number           // 4 bytes
  /** Location of Zip64 EOCD Locator in file */
  _offset: number
}
export const ZIP64_EOCD_LOCATOR_SIZE = 20
export const ZIP64_EOCD_LOCATOR_SIGNATURE = 0x07064b50

/** Zip64 end of central directory record */
export interface Zip64EOCD {
  /** Should be 0x06064b50 */
  signature: number                 // 4 bytes
  /** Size of zip64 end of central directory record. Does not include 12 bytes
   * of signature + size - minimum value is 44 */
  size: number                      // 8 bytes
  /** Version made by */
  versionMadeBy: ZipVersionMadeBy   // 2 bytes
  /** Version needed to extract */
  versionExtract: number            // 2 bytes
  /** Number of this disk */
  diskNum: number                   // 4 bytes
  /** Number of the disk with the start of the central directory */
  diskWithCD: number                // 4 bytes
  /** Total number of entries in the central directory on this disk */
  entriesOnDisk: number             // 8 bytes
  /** Total number of entries in the central directory */
  entriesTotal: number              // 8 bytes
  /** Size of the central directory */
  sizeofCD: number                  // 8 bytes
  /** Offset of start of central directory with respect to the starting disk number */
  offsetCD: number                  // 8 bytes
  /** Zip64 extensible data sector */
  zip64ExtData: ArrayBuffer            // Value of size after subtracting minimum
}
/** Minimum size of Zip64EOCD. Actual struct may be larger if extensible data
 * field is present. */
export const ZIP64_EOCD_MIN_SIZE = 56
/** Minimum value of the Zip64EOCD.size. Size value does not include self or signature. */
export const ZIP64_EOCD_SIZE_MIN_VALUE = ZIP64_EOCD_MIN_SIZE - 12
export const ZIP64_EOCD_SIGNATURE = 0x06064b50

/** Structure representing a parsed Central Directory entry */
export interface ZipCDEntry {
  /** Should be 0x02014b50 */
  signature: number                     // 4 bytes
  /** Version made by */
  versionMadeBy: ZipVersionMadeBy       // 2 bytes
  /** Version needed to extract */
  versionExtract: number                // 2 bytes
  /** General purpose bit flag */
  generalBitFlags: ZipGeneralFlags      // 2 bytes
  /** Compression method */
  compressionMethod: CompressionMethod  // 2 bytes
  /** Last modified file time, in MS-DOS format */
  lastModTime: number                   // 2 bytes
  /** Last modified file date, in MS-DOS format */
  lastModDate: number                   // 2 bytes
  /** CRC-32 */
  crc32: number                         // 4 bytes
  /** Compressed size of file */
  compressedSize: number                // 4 bytes
  /** Uncompressed size of file */
  uncompressedSize: number              // 4 bytes
  /** Length of filename field in bytes */
  filenameLength: number                // 2 bytes
  /** Length of extra field in bytes */
  extraFieldLength: number              // 2 bytes
  /** Length of file comment in bytes */
  commentLength: number                 // 2 bytes
  /** Disk number start */
  diskNumberStart: number               // 2 bytes
  /** Internal file attributes */
  internalFileAttributes: number        // 2 bytes
  /** External file attributes */
  externalFileAttributes: number        // 4 bytes
  /** Relative offset of local header */
  localHeaderOffset: number             // 4 bytes
  /** File name */
  filename: string                      // Value of filenameLength
  /** Extra field */
  extraField: ZipExtraFieldMapping      // Value of extraFieldLength
  /** File comment */
  comment: string                       // Value of commentLength
  /** @hidden Entry filename, as a raw ArrayBuffer. Use for calculating the CRC
    for Infozip Unicode filename extra fields. */
  _filenameBuffer: ArrayBuffer
}
export const ZIP_CDENTRY_MIN_SIZE = 46
export const ZIP_CDENTRY_SIGNATURE = 0x02014b50

export interface ZipLocalFileHeader {
  /** Should be 0x04034b50 */
  signature: number                     // 4 bytes
  /** Version needed to extract */
  versionExtract: number                // 2 bytes
  /** General purpose bit flag */
  generalBitFlags: ZipGeneralFlags      // 2 bytes
  /** Compression method */
  compressionMethod: CompressionMethod  // 2 bytes
  /** Last modified file time, in MS-DOS format */
  lastModTime: number                   // 2 bytes
  /** Last modified file date, in MS-DOS format */
  lastModDate: number                   // 2 bytes
  /** CRC-32 */
  crc32: number                         // 4 bytes
  /** Compressed size of file */
  compressedSize: number                // 4 bytes
  /** Uncompressed size of file */
  uncompressedSize: number              // 4 bytes
  /** Length of filename field in bytes */
  filenameLength: number                // 2 bytes
  /** Length of extra field in bytes */
  extraFieldLength: number              // 2 bytes
  /** File name */
  filename: string                      // Size of filenameLength
  /** Extra field */
  extraField: ZipExtraFieldMapping      // Size of extraFieldLength
}
export const ZIP_LOCAL_FILE_HEADER_MIN_SIZE = 30
export const ZIP_LOCAL_FILE_HEADER_SIGNATURE = 0x04034b50


/* Utility functions specific to structs
 ****************************************/

/** Convertions MS-DOS datetime format to Javascript Date */
export function dateFromMSDOS(date: number, time: number): Date {
  return new Date(
    ((date & 0b1111111000000000) >>> 9) + 1980,  // Year
    ((date & 0b0000000111100000) >>> 5) - 1,  // Month (1-indexed)
    date & 0b0000000000011111,  // Day
    ((time & 0b1111100000000000) >>> 11),  // Hour
    ((time & 0b0000011111100000) >>> 5),  // Minute
    (time & 0b0000000000011111) * 2  // Seconds (2-second precision)
  )
}

/** Read a 64-bit little-endian value from view at offset as a Javascript number.

  @throws {ZipRangeError} when value exceeds MAX_SAFE_INTEGER
*/
export function getUint64LEAsNumber(view: DataView, offset: number): number {
  const lowDWord = view.getUint32(offset, LITTLE_ENDIAN)
  const highDWord = view.getUint32(offset + 4, LITTLE_ENDIAN)
  if (highDWord > MAX_SAFE_INT_HIGH_DWORD) {
    // Zip files with offsets/lengths > 8PB not supported
    throw new ZipRangeError('Zip64 value exceeds maximum safe integer')
  }
  return highDWord * 2**32 + lowDWord
}

/** Parse the Info-ZIP Unicode Path Extra Field (0x7075). Returns three
  element tuple consisting of the field version, CRC32 of header's filename
  this field was created for, and the filename contained in this field. */
function parseExtraFieldInfozipUnicode(field: ArrayBuffer): ExtraFieldInfozipUnicode {
  if (field.byteLength < 5) {
    throw new ZipInvalidError(
      'corruptArchive', 'Could not parse Infozip unicode field'
    )
  }

  const view = new DataView(field)
  const version = view.getUint8(0)
  const entryNameCRC32 = view.getUint32(1, LITTLE_ENDIAN)
  const filename = decodeBufferSlice(field, 5)
  return {version, entryNameCRC32, filename}
}

/** Parse a Zip64 Extended Information Extra Field */
function parseExtraFieldZip64(field: ArrayBuffer): ExtraFieldZip64 {
  const view = new DataView(field)
  let diskNumberStart: number | undefined
  let localHeaderOffset: number | undefined
  let compressedSize: number | undefined
  let uncompressedSize: number

  switch(field.byteLength) {
    // Doing switch case fallthrough on purpose here
    /* eslint-disable no-fallthrough */
    case 28:
      diskNumberStart = view.getUint32(24, LITTLE_ENDIAN)
    case 24:
      localHeaderOffset = getUint64LEAsNumber(view, 16)
    case 16:
      compressedSize = getUint64LEAsNumber(view, 8)
    /* eslint-enable no-fallthrough */
    case 8:
      uncompressedSize = getUint64LEAsNumber(view, 0)
      break
    default:
      throw new ZipInvalidError(
        'corruptArchive', `Zip64 extra field unknown length (${field.byteLength})`
      )
  }

  return {uncompressedSize, compressedSize, localHeaderOffset, diskNumberStart}
}

/** Extract extra fields from the given buffer

  @param buffer  An ArrayBuffer that contains the extraField data
  @param offset  Offset of the start of the extraField data in buffer
  @param length: Size of the extraField data
*/
export function extraFieldMappingFromBuffer(
  buffer: ArrayBuffer, offset: number, length: number
): ZipExtraFieldMapping {
  const extraField: ZipExtraFieldMapping = {}
  const extraFieldView = new DataView(buffer, offset, length)
  let extraFieldPointer = 0
  while (extraFieldPointer < length) {
    const extraFieldEntryId = extraFieldView.getUint16(extraFieldPointer, LITTLE_ENDIAN)
    const extraFieldEntryLength = extraFieldView.getUint16(extraFieldPointer + 2, LITTLE_ENDIAN)
    extraField[extraFieldEntryId] = buffer.slice(
      extraFieldPointer + offset + 4,
      extraFieldPointer + offset + 4 + extraFieldEntryLength
    )
    extraFieldPointer += 4 + extraFieldEntryLength
  }

  extraField.zip64 = extraField[ExtraFieldIds.Zip64] ?
    parseExtraFieldZip64(extraField[ExtraFieldIds.Zip64]) : undefined
  extraField.infozipUnicode = extraField[ExtraFieldIds.InfoZIPPath] ?
    parseExtraFieldInfozipUnicode(extraField[ExtraFieldIds.InfoZIPPath]) : undefined

  return extraField
}

/** Create general bit flags object from raw data

  @param flagsRaw - Raw value of flags field
  @returns Flags as a parsed ZipGeneralFlags object

  @throws {ZipUnsupportedError} When an unrecognized bit is set in the raw flags
*/
export function generalBitFlagsFromRaw(flagsRaw: number): ZipGeneralFlags {
  // Check for unknown flags
  if (flagsRaw & 0b1101011110010000) {
    throw new ZipUnsupportedError('flags', flagsRaw)
  }
  return {
    encrypted: (flagsRaw & 0b0000000000000001) !== 0,
    compressionSettings: (flagsRaw & 0b0000000000000110) >>> 1,
    descriptorFollows: (flagsRaw & 0b0000000000001000) !== 0,
    patched: (flagsRaw & 0b0000000000100000) !== 0,
    strongEncryption: (flagsRaw & 0b0000000001000000) !== 0,
    languageEncoding: (flagsRaw & 0b0000100000000000) !== 0,
    masked: (flagsRaw & 0b0010000000000000) !== 0,
    _raw: flagsRaw
  }
}
