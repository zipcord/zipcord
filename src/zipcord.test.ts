/** Tests for the main Zipcord class */

import Zipcord from './'
import { ZipInvalidError, ZipReadError } from './errors'
import { ZipcordRaw } from './zipcord-raw'
import {
  getEntryOrFail, getFile,
  DUMMY_DECODER, HELLO_FILENAME, HELLO_TEXT, MOCK_ZIPFILE_NOLENGTH_URL,
  MOCK_ZIPFILE_NORANGE_URL, MOCK_ZIPFILE_URL
} from './testdata/index'


// Functionality tests

test('Open a zip file and read a file', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))
  expect(zip.filenames).toEqual(['Hello.txt', 'images/', 'images/smile.gif'])
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
})

test('Read from an URL', async () => {
  // Disable extra buffering to prevent loading as buffer right away
  const zip = await Zipcord.open(MOCK_ZIPFILE_URL, {commentBufferSize: 0})
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
})

test('Automatic fallback to downloading source when Range requests unsupported', async () => {
  const zip = await Zipcord.open(MOCK_ZIPFILE_NORANGE_URL, {commentBufferSize: 0})
  expect(zip.readingFromUrl).toBe(false)
})

test('Force downgrading from URL to buffer', async () => {
  const zip = await Zipcord.open(MOCK_ZIPFILE_URL, {commentBufferSize: 0})
  expect(zip.readingFromUrl).toBe(true)
  await zip.disableUrlReading()
  expect(zip.readingFromUrl).toBe(false)

  // Should still be allowed to force buffer reading when rangeRequired set
  const zipRequired = await Zipcord.open(
    MOCK_ZIPFILE_URL,
    {commentBufferSize: 0, rangeRequired: true}
  )
  expect(zipRequired.readingFromUrl).toBe(true)
  await zipRequired.disableUrlReading()
  expect(zipRequired.readingFromUrl).toBe(false)
})

test('Maximum size of URL download fallback', async () => {
  const zip = await Zipcord.open(MOCK_ZIPFILE_NORANGE_URL, {
    commentBufferSize: 0,
    maxFallbackSize: 1024
  })
  expect(zip.readingFromUrl).toBe(false)

  await expect(() => Zipcord.open(MOCK_ZIPFILE_NORANGE_URL, {
    commentBufferSize: 0,
    maxFallbackSize: 16
  }) ).rejects.toThrow(ZipReadError)
})

test('ZipcordRaw tries decoders until one succeeds', async () => {
  const zip = await ZipcordRaw.open(
    getFile('deflate.zip'),
    {
      decoderSetup: [
        // First setup should fail
        async () => Promise.reject('Failed initializer'),
        // then Zipcord should try the next and succeed
        async () => DUMMY_DECODER
      ]
    }
  )

  await expect(zip.decoder).resolves.toEqual(DUMMY_DECODER)
})

test('Zipcord (default export) uses fallback if decoder in options fails', async () => {
  const zip = await Zipcord.open(
    getFile('deflate.zip'),
    { decoderSetup: async () => Promise.reject('Failed initializer') }
  )

  await expect(zip.decoder).resolves.toMatchSnapshot('zip.decoder')
  await expect(getEntryOrFail(zip, HELLO_FILENAME).buffer())
    .resolves.toMatchSnapshot('entry.buffer')
})

test('Throws error when creating ZipcordRaw without a decoder', async () => {
  expect(() => new ZipcordRaw()).toThrowErrorMatchingSnapshot()
})

test('Throws error when maximum size set for fallback but Content-Length unknown', async () => {
  await expect(Zipcord.open(MOCK_ZIPFILE_NOLENGTH_URL, {
    commentBufferSize: 0,
    maxFallbackSize: 1024
  })).rejects.toThrowErrorMatchingSnapshot()
})


// Archive contents tests

test('Archive with comment', async () => {
  const zip = await Zipcord.open(getFile('archive_comment.zip'))
  expect(zip.comment).toEqual('file comment')
  // Ensure appended data not read as comment
  const zipAppend = await Zipcord.open(getFile('all_appended_bytes.zip'))
  expect(zipAppend.comment).toBeUndefined()
})

test('Verify file structure parsing', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))
  expect(zip.eocd).toMatchSnapshot('EOCD')
  expect(zip.centralDirectory).toMatchSnapshot('Central directory')
})

test('Empty zip', async () => {
  const zip = await Zipcord.open(getFile('empty.zip'))
  expect(zip.entries.size).toEqual(0)
})

test('Read a Zip64', async () => {
  const zip = await Zipcord.open(getFile('zip64.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
})

test('Verify Zip64 structure parsing', async () => {
  const zip = await Zipcord.open(getFile('zip64.zip'))
  expect(zip.zip64EOCDLocator).toMatchSnapshot('Zip64 EOCD locator')
  expect(zip.zip64EOCD).toMatchSnapshot('Zip64 EOCD')
})

test('Appended data', async () => {
  const zip = await Zipcord.open(getFile('all_appended_bytes.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
  const zip64 = await Zipcord.open(getFile('zip64_appended_bytes.zip'))
  const text64 = await getEntryOrFail(zip64, HELLO_FILENAME).text()
  expect(text64).toEqual(HELLO_TEXT)
})

test('Prepended data', async () => {
  const zip = await Zipcord.open(getFile('all_prepended_bytes.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
  const zip64 = await Zipcord.open(getFile('zip64_prepended_bytes.zip'))
  const text64 = await getEntryOrFail(zip64, HELLO_FILENAME).text()
  expect(text64).toEqual(HELLO_TEXT)
})

test('Missing bytes', async () => {
  await expect(() => Zipcord.open(getFile('invalid/all_missing_bytes.zip')))
    .rejects.toThrow(ZipInvalidError)
})

test('Extra field attributes in central directory', async () => {
  const zip = await Zipcord.open(getFile('extra_attributes.zip'))
  const entry = getEntryOrFail(zip, HELLO_FILENAME)

  const cdField5455 = entry.cdEntry.extraField[0x5455]
  expect(cdField5455).toBeDefined()
  expect(cdField5455.byteLength).toEqual(5)
  const cdField7875 = entry.cdEntry.extraField[0x7875]
  expect(cdField7875).toBeDefined()
  expect(cdField7875.byteLength).toEqual(11)
})

test('Zero-length file is invalid', async () => {
  await expect( () => Zipcord.open( new ArrayBuffer(0) ) ).rejects.toThrow(ZipInvalidError)
})
