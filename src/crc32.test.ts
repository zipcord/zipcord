/** Tets for CRC-32 calculation */

import { crc32 } from './crc32'


test('Basic CRC-32 calculation', () => {
    const buffer = Uint8Array.from([0, 1, 2, 3, 4, 5, 6, 7])
    const crc = crc32(buffer)
    expect(crc).toBe(0x88aa689f)
})
