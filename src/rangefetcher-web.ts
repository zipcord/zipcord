/** Exports a class that loads segments of data from a resource */

export { RangeFetcherBase as RangeFetcher, ALWAYS_FALLBACK } from './rangefetcher-common.js'
