/** Entry point for Zipcord (Node). This module is replaced by `index-web.ts`
when building for a browser.

In addition to the exports the web entry provides, it also exports the
[[`ZipcordNode`]] class, Zipcord enhanced with NodeJS features like native
zlib decoding.

@packageDocumentation
*/

export * from './index-common.js'

export {
  ZipcordNode, ZipcordNode as Zipcord, ZipcordNode as default, nodeConfigSetup
} from './zipcord-node.js'
// Also export JS-based decoder if you don't want to use zlib for some reason
export { ZipcordFflate, fflateDecoderSetup } from './zipcord-fflate.js'
