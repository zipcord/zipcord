/** Tests for reading ranges from URLs/buffers */

import { ZipRangeError, ZipReadError } from './errors'
import { RangeFetcher } from './rangefetcher'
import { decodeBufferSlice } from './util'
import {
  MOCK_FETCHRANGE_URL, MOCK_HTTP_404_URL, MOCK_HTTP_500_URL, MOCK_NOLENGTH_URL,
  MOCK_NORANGE_URL
} from './testdata/index'


test('Basic range reading', async () => {
  const fetcher = new RangeFetcher(MOCK_FETCHRANGE_URL)

  const dataAll = await fetcher.readArrayBuffer(0)
  expect(decodeBufferSlice(dataAll)).toEqual('0123456789')
  const dataToEnd = await fetcher.readArrayBuffer(5)
  expect(decodeBufferSlice(dataToEnd)).toEqual('56789')
  const dataStart = await fetcher.readArrayBuffer(0, 5)
  expect(decodeBufferSlice(dataStart)).toEqual('01234')
  const dataOffset = await fetcher.readArrayBuffer(5, 5)
  expect(decodeBufferSlice(dataOffset)).toEqual('56789')

  await expect(() => fetcher.readArrayBuffer(5, 6)).rejects.toThrow(ZipRangeError)
})

test('Reading from an ArrayBuffer', async () => {
  const testBuffer = Uint8Array.from([48, 49, 50, 51, 52, 53, 54, 55, 56, 57])
  const fetcher = new RangeFetcher(testBuffer)
  const responseData = await fetcher.readArrayBuffer(2, 4)
  expect(decodeBufferSlice(responseData, 0, 4)).toEqual('2345')
})

// Need to find a way to mock ReadableStream to test
test.todo('Reading as a ReadableStream')

test('Loading source file information', async () => {
  const fetcherRange = new RangeFetcher(MOCK_FETCHRANGE_URL)
  expect(fetcherRange.headersLoaded).toBe(false)
  expect(fetcherRange.sourceSize).toBeUndefined()
  await fetcherRange.loadSourceHeaders()
  expect(fetcherRange.headersLoaded).toBe(true)
  expect(fetcherRange.rangeSupported).toBe(true)
  expect(fetcherRange.readingFromUrl).toBe(true)
  expect(fetcherRange.sourceSize).toBe(10)

  const fetcherNoRange = new RangeFetcher(MOCK_NORANGE_URL)
  expect(fetcherNoRange.headersLoaded).toBe(false)
  expect(fetcherNoRange.sourceSize).toBeUndefined()
  await fetcherNoRange.loadSourceHeaders()
  expect(fetcherNoRange.headersLoaded).toBe(true)
  expect(fetcherNoRange.rangeSupported).toBe(false)
  // While Range requests are not supported until an entry is read or URL
  // reading is disabled the source is still an URL
  expect(fetcherNoRange.readingFromUrl).toBe(true)
  expect(fetcherNoRange.sourceSize).toBe(10)
})

test('Automatically downgrade to buffer reading when Range unsupported', async () => {
  const fetcher = new RangeFetcher(MOCK_NORANGE_URL)
  expect(fetcher.readingFromUrl).toBe(true)
  const data = await fetcher.readArrayBuffer(0)
  expect(decodeBufferSlice(data)).toEqual('0123456789')
  expect(fetcher.rangeSupported).toBe(false)
  expect(fetcher.readingFromUrl).toBe(false)
})

test('Automatic download fallback size limit', async () => {
  const fetcher = new RangeFetcher(MOCK_NORANGE_URL, false, 1024)
  expect(fetcher.readingFromUrl).toBe(true)
  await fetcher.readArrayBuffer(0)
  expect(fetcher.readingFromUrl).toBe(false)

  const fetcherLimit = new RangeFetcher(MOCK_NORANGE_URL, false, 1)
  await expect(() => fetcherLimit.readArrayBuffer(0)).rejects.toThrow(ZipReadError)
})

test('Downgrading fails when no size headers present and maxFallbackSize set', async () => {
  const fetcher = new RangeFetcher(MOCK_NOLENGTH_URL, false, 1)
  await expect(() => fetcher.readArrayBuffer(0)).rejects.toThrow(ZipReadError)
})

test('Manually disabling URL streaming', async () => {
  const fetcher = new RangeFetcher(MOCK_FETCHRANGE_URL)
  expect(fetcher.readingFromUrl).toBe(true)
  await fetcher.disableUrlReading()
  expect(fetcher.readingFromUrl).toBe(false)
})

test('Can still force disable URL reading when rangeRequired set', async () => {
  const fetcher = new RangeFetcher(MOCK_FETCHRANGE_URL, true)
  expect(fetcher.readingFromUrl).toBe(true)
  await fetcher.disableUrlReading()
  expect(fetcher.readingFromUrl).toBe(false)
})

test('Cannot read from an URL source when rangeRequired set but Range unsupported', async () => {
  const fetcher = new RangeFetcher(MOCK_NORANGE_URL, true)
  expect(fetcher.readingFromUrl).toBe(true)
  await expect(() => fetcher.readArrayBuffer(5)).rejects.toThrow(ZipReadError)
})

test('HTTP 4xx/5xx responses throw error', async () => {
  const fetcher404 = new RangeFetcher(MOCK_HTTP_404_URL)
  await expect(() => fetcher404.readArrayBuffer(0)).rejects.toThrow(ZipReadError)
  const fetcher500 = new RangeFetcher(MOCK_HTTP_500_URL)
  await expect(() => fetcher500.readArrayBuffer(0)).rejects.toThrow(ZipReadError)
})
