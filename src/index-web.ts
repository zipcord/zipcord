/** Entry point for Zipcord (web).

  @packageDocumentation
*/

export * from './index-common.js'

export {
  ZipcordFflate, ZipcordFflate as Zipcord, ZipcordFflate as default, fflateDecoderSetup
} from './zipcord-fflate.js'
