/** CRC-32 calculation  @packageDocumentation */

// This file is modified from crc32.js in Pako. Copyright notice included below
// as required.
// Original: https://github.com/nodeca/pako/blob/master/lib/zlib/crc32.js

// (C) 1995-2013 Jean-loup Gailly and Mark Adler
// (C) 2014-2017 Vitaly Puzrin and Andrey Tupitsin
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//   claim that you wrote the original software. If you use this software
//   in a product, an acknowledgment in the product documentation would be
//   appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//   misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.


// Use ordinary array, since untyped makes no boost here
/** Cached lookup table */
let lookupTable: number[] | undefined
/** Calculates the lookup table for the CRC-32 calculations. Caches the array
 * on the first run and returns it on following runs.
 */
function getLookupTable(): number[] {
  // Used cached table if available
  if (lookupTable) {
    return lookupTable
  }

  lookupTable = []
  for (let tableIndex = 0; tableIndex < 256; tableIndex++) {
    let value = tableIndex
    for (let k = 0; k < 8; k++) {
      value = ((value & 1) ? (0xEDB88320 ^ (value >>> 1)) : (value >>> 1))
    }
    lookupTable[tableIndex] = value
  }

  return lookupTable
}


/** Calculates the CRC-32 of a buffer
  @param buffer - Buffer for the CRC calculation
  @param crc - Initial CRC value. Use to process an input source in chunks,
    passing the CRC calculated so far. Optional, don't use for first/only call.
 */
export function crc32(buffer: Uint8Array, crc = 0): number {
  const table = getLookupTable()

  /* This algorithm originally looked like it was ported from zlib's C code
    straight into ES3-era Javascript. I modernized it a bit and some quick-and-
    dirty benchmarking showed no difference in performance.

    Pako's authors say it can't be optimized much further and I believe them
    because they're smarter than me. If CRC-32 performance becomes an actual
    issue it may be worth investigating implementing it in WASM instead.
  */
  crc ^= -1
  for (const byte of buffer) {
    crc = (crc >>> 8) ^ table[(crc ^ byte) & 0xFF]
  }

  return (crc ^ -1) >>> 0   // >>> 0 forces value to be treated as unsigned
}
