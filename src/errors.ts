/** Errors that can be raised by Zipcord. All errors extend from [[`ZipError`]].

  @packageDocumentation
*/

import type { CompressionMethod } from './structs.js'


type ZipInvalidCauses = 'corruptData' | 'corruptArchive' | 'crc32' | 'length'
type ZipReadErrorCauses = 'contentSize' | 'httpStatus' | 'rangeRequired' | 'tooBig'
type ZipUnsupportedFeatures = 'compression' | 'encryption' | 'environment' | 'flags' | 'multipart'

/** Base class for expected problems */
export abstract class ZipError extends Error {}

/** The zip file is corrupt, has wrong information, or is otherwise invalid */
export class ZipInvalidError extends ZipError {
  readonly cause: ZipInvalidCauses

  /** Create error for an archive with an invalid structure */
  constructor(cause: 'corruptArchive', details: string | Error)
  /** Create error for an archive entry with an invalid Deflate bitstream */
  constructor(cause: 'corruptData', details: string | Error)
  /** Create error for an archive entry with an invalid CRC32 */
  constructor(cause: 'crc32', details: [expected: number, received: number])
  /** Create error for an archive entry with an invalid extracted length */
  constructor(cause: 'length', details: [expected: number, received: number])
  /** Create error for an archive that is invalid in some way */
  constructor(cause: ZipInvalidCauses, details: string | Error | [expected: number, received: number]
  ) {
    switch (cause) {
      case 'corruptArchive': {
        super('Archive corrupt: ' + details)
        break
      }
      case 'corruptData': {
        super('Compressed data corrupt: ' + details)
        break
      }
      case 'crc32': {
        const [expected, received] = details as number[]
        super(`Extracted CRC32 0x${received.toString(16)}, expected 0x${expected.toString(16)}`)
        break
      }
      case 'length': {
        const [expected, received] = details as number[]
        super(`Extracted ${received} bytes, expected ${expected} bytes`)
        break
      }
      default:
        super(`Archive invalid: Unknown cause [${cause}]`)
    }

    this.cause = cause
  }
}

/** A value was outside of an allowable range (e.g. reading beyond end of archive) */
export class ZipRangeError extends ZipError {}

/** Problem reading zip archive */
export class ZipReadError extends ZipError {
  /** Text symbol representing the cause of the read error */
  readonly cause: ZipReadErrorCauses

  /** Create error for missing an HTTP header specifying length */
  constructor(cause: 'contentSize')
  /** Create error for an HTTP error status */
  constructor(cause: 'httpStatus', response: Response)
  /** Create error for rangeRequired set but no `Range` header in response */
  constructor(cause: 'rangeRequired')
  /** Create error for archive size exceeding maxFallbackSize on fallback download */
  constructor(cause: 'tooBig', size: number, maxFallbackSize: number)
  /** Create error. Additional parameters depend on cause. */
  constructor(cause: ZipReadErrorCauses, responseOrSize?: Response | number, maxFallbackSize?: number) {
    switch (cause) {
      case 'contentSize': {
        super('Content-Length or Content-Range not found in HTTP headers')
        break
      }
      case 'httpStatus': {
        super(`HTTP ${(responseOrSize as Response).status} response from server`)
        break
      }
      case 'rangeRequired': {
        super('Range requests are required but are unsupported by server')
        break
      }
      case 'tooBig': {
        super(
          'Archive too large for automatic download fallback ' +
          // responseOrSize is a number in this case
          `(${responseOrSize} bytes, maxFallbackSize=${maxFallbackSize})`
        )
        break
      }
      default:
        super(`Read error: Unknown [${cause}]`)
    }

    this.cause = cause
  }
}

/** [[`ZipcordRaw`]]/[[`ZipEntry`]] field accessed before [[`initialize`]] called */
export class ZipUninitializedError extends ZipError {
  constructor (message?: string) {
    super(message ?? 'Must initialize object before reading this info')
  }
}

/** A required but unsupported feature is unavailable, e.g. archive entry
  encryption or `ReadableStream` not present when calling [[`bufferStream`]] */
export class ZipUnsupportedError extends ZipError {
  readonly feature: ZipUnsupportedFeatures
  /** Create error for unknown compression method on file entry */
  constructor(feature: 'compression', details: CompressionMethod)
  /** Create error for attempting to extract an encrypted file entry */
  constructor(feature: 'encryption')
  /** Create error for problem with runtime enviromnent */
  constructor(feature: 'environment', details: string)
  /** Create error for unknown values in flags bitfield of file entry */
  constructor(feature: 'flags', details: number)
  /** Create error for attempting to open a multipart archive */
  constructor(feature: 'multipart')
  /** Create error for a required but unavailable feature */
  constructor(feature: ZipUnsupportedFeatures, details?: CompressionMethod | Headers | number | string) {
    const prefix = 'Unsupported required feature: '

    switch (feature) {
      case 'compression': {
        // details is the numeric value of the compression method field
        super(prefix + `Zip compression method ${details}`)
        break
      }
      case 'encryption': {
        super(prefix + 'Archive entry encrypted')
        break
      }
      case 'environment': {
        // details is a string
        super(prefix + details)
        break
      }
      case 'flags': {
        // details is the raw value of the bitfield
        super(prefix + `Unknown flags (0x${details?.toString(16)} 0b${details?.toString(2)})`)
        break
      }
      case 'multipart': {
        super(prefix + 'Multipart archive')
        break
      }
      default:
        super(prefix + `Unknown [${feature}]`)

    }
    this.feature = feature
  }
}

/** Error with Zipcord itself. Usually represents a bug in Zipcord, however
  it can also be thrown for an improper configuration (e.g. could not
  initialize the Deflate decoder).
 */
export class ZipcordError extends ZipError {}
