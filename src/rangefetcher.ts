/** Node-specific implementation of RangeFetcher. This module is replaced by
`rangefetcher-web.ts` when building for a browser.

Adds a method that returns a Node stream for the underlying data. Not available
on web targets.

Currently requires polyfills for fetch and AbortController. In the future this
could be expanded to implement HTTP requests in node using native functions.
*/

import { Duplex } from 'stream'
import type { Readable } from 'stream'

import { RangeFetcherBase } from './rangefetcher-common.js'
import { sourceIsUrl, HTTP_PARTIAL_CONTENT } from './util.js'


export { ALWAYS_FALLBACK } from './rangefetcher-common.js'

export class RangeFetcher extends RangeFetcherBase {
  /** Similar to RangeFetcherBase.readStream but returns a Node stream rather
    than a ReadableStream */
  public async readStreamNode(offset: number, length?: number): Promise<Readable> {
    // If sourceSize was not set yet will check this again after getting data
    if (this.sourceSize !== undefined) {
      this.checkRangeBounds(offset, length)
    }

    let reader: Readable
    if (sourceIsUrl(this.currentSource)) {
      const response = await this.getFetchRangeResponse(offset, length, true)
      if (response.status === HTTP_PARTIAL_CONTENT) {
        // Typescript types Response.body as a ReadableStream but fetch-node
        // provides a Node Readable instead
        reader = response.body as unknown as Readable

      } else {
        // Response was successful but not a range, switch to buffer reading
        await this.disableUrlReading(response)
        reader = await this.readStreamNode(offset, length)
      }
    } else {  // Buffer source
      const sourceBuffer = new Uint8Array(this.getSourceBufferRange(offset, length))
      const sourceStream = new Duplex()
      sourceStream.push(sourceBuffer)
      sourceStream.push(null)

      reader = sourceStream
    }

    return reader
  }
}
