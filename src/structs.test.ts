/** Tests for data structure utility functions */

import { ZipInvalidError, ZipRangeError, ZipUnsupportedError } from './errors'
import {
  ExtraFieldIds, LITTLE_ENDIAN,
  dateFromMSDOS, extraFieldMappingFromBuffer, generalBitFlagsFromRaw,
  getUint64LEAsNumber
} from './structs'


test('dateFromMSDOS()', () => {
  const dosDate = 4608 + 64 + 14   // Date zip specification entered public domain
  const dosTime = 8192 + 640 + 21  // lol
  const datetime = new Date(1989, 1, 14, 4, 20, 42)
  expect(dateFromMSDOS(dosDate, dosTime)).toEqual(datetime)
})

test('extraFieldMappingFromBuffer()', () => {
  const extraBuffer = new Uint8Array([
    0xff, 0xff,                                       // Dummy data
    0x2a, 0x00, 0x04, 0x00, 0xff, 0xff, 0xff, 0x7f,   // Field ID 42
    0x45, 0x00, 0x02, 0x00, 0xff, 0x7f                // Field ID 69
  ]).buffer

  const mapping = extraFieldMappingFromBuffer(extraBuffer, 2, 14)
  expect(mapping[42]).toBeDefined()
  expect(mapping[42].byteLength).toEqual(4)
  expect(new DataView(mapping[42]).getUint32(0, LITTLE_ENDIAN)).toEqual(0x7fffffff)
  expect(mapping[69]).toBeDefined()
  expect(mapping[69].byteLength).toEqual(2)
  expect(new DataView(mapping[69]).getUint16(0, LITTLE_ENDIAN)).toEqual(0x7fff)
})

test('extraFieldMappingFromBuffer() + parseExtraFieldInfozipUnicode()', () => {
  const unicodeName = "💩"
  const nameCRC32 = 0x12345678

  const unicodeField = new ArrayBuffer(13)
  const view = new DataView(unicodeField)
  view.setUint16(0, ExtraFieldIds.InfoZIPPath, LITTLE_ENDIAN)
  view.setUint16(2, 9, LITTLE_ENDIAN)
  view.setUint8(4, 1)
  view.setUint32(5, nameCRC32, LITTLE_ENDIAN)
  view.setUint32(9, 0xf09f92a9)   // Raw byte value of 💩

  const extra = extraFieldMappingFromBuffer(unicodeField, 0, unicodeField.byteLength)
  // Raw field
  expect(extra[ExtraFieldIds.InfoZIPPath]).toBeDefined()
  expect(extra[ExtraFieldIds.InfoZIPPath]?.byteLength).toEqual(9)
  // Parsed field
  expect(extra.infozipUnicode).toBeDefined()
  expect(extra.infozipUnicode?.entryNameCRC32).toEqual(nameCRC32)
  expect(extra.infozipUnicode?.filename).toEqual(unicodeName)

  // Invalid field
  expect(() => extraFieldMappingFromBuffer(unicodeField.slice(0, 8), 0, 8)).toThrow(ZipInvalidError)
})

test('extraFieldMappingFromBuffer() + parseExtraFieldZip64()', () => {
  const SAMPLE_UNCOMPRESSED_SIZE = Number.MAX_SAFE_INTEGER
  const SAMPLE_COMPRESSED_SIZE = Number.MAX_SAFE_INTEGER - 1
  const SAMPLE_OFFSET = Number.MAX_SAFE_INTEGER - 2
  const SAMPLE_DISKNUM = 69

  const extraBuffer = new ArrayBuffer(32)
  const extraView = new DataView(extraBuffer)
  extraView.setUint16(0, ExtraFieldIds.Zip64, LITTLE_ENDIAN)
  // Field length set per test
  extraView.setBigUint64(4, BigInt(SAMPLE_UNCOMPRESSED_SIZE), LITTLE_ENDIAN)
  extraView.setBigUint64(12, BigInt(SAMPLE_COMPRESSED_SIZE), LITTLE_ENDIAN)
  extraView.setBigUint64(20, BigInt(SAMPLE_OFFSET), LITTLE_ENDIAN)
  extraView.setUint32(28, SAMPLE_DISKNUM, LITTLE_ENDIAN)

  // Uncompressed size only
  extraView.setUint16(2, 8, LITTLE_ENDIAN)
  const extra8 = extraFieldMappingFromBuffer(extraBuffer, 0, 12)
  expect(extra8[ExtraFieldIds.Zip64]?.byteLength).toEqual(8)
  expect(extra8.zip64).toBeDefined()
  expect(extra8.zip64).toEqual({uncompressedSize: SAMPLE_UNCOMPRESSED_SIZE})

  // Uncompressed + compressed
  extraView.setUint16(2, 16, LITTLE_ENDIAN)
  const extra16 = extraFieldMappingFromBuffer(extraBuffer, 0, 20)
  expect(extra16[ExtraFieldIds.Zip64]?.byteLength).toEqual(16)
  expect(extra16.zip64).toEqual({
    uncompressedSize: SAMPLE_UNCOMPRESSED_SIZE,
    compressedSize: SAMPLE_COMPRESSED_SIZE
  })

  // Uncompressed, compressed, and offset
  extraView.setUint16(2, 24, LITTLE_ENDIAN)
  const extra24 = extraFieldMappingFromBuffer(extraBuffer, 0, 28)
  expect(extra24[ExtraFieldIds.Zip64]?.byteLength).toEqual(24)
  expect(extra24.zip64).toEqual({
    uncompressedSize: SAMPLE_UNCOMPRESSED_SIZE,
    compressedSize: SAMPLE_COMPRESSED_SIZE,
    localHeaderOffset: SAMPLE_OFFSET
  })

  // Uncompressed, compressed, offset, and disk number
  extraView.setUint16(2, 28, LITTLE_ENDIAN)
  const extra28 = extraFieldMappingFromBuffer(extraBuffer, 0, 32)
  expect(extra28[ExtraFieldIds.Zip64]?.byteLength).toEqual(28)
  expect(extra28.zip64).toEqual({
    uncompressedSize: SAMPLE_UNCOMPRESSED_SIZE,
    compressedSize: SAMPLE_COMPRESSED_SIZE,
    localHeaderOffset: SAMPLE_OFFSET,
    diskNumberStart: SAMPLE_DISKNUM
  })

  // Invalid Zip64 field
  extraView.setUint16(2, 4, LITTLE_ENDIAN)
  expect(() => extraFieldMappingFromBuffer(extraBuffer, 0, 8)).toThrow(ZipInvalidError)
})

test('generalBitFlagsFromRaw()', () => {
  const bitfield = 0b0010100001101111
  const flags = generalBitFlagsFromRaw(bitfield)
  expect(flags).toEqual(expect.objectContaining({
    encrypted: true,
    compressionSettings: 3,
    descriptorFollows: true,
    patched: true,
    strongEncryption: true,
    languageEncoding: true,
    masked: true,
  }))

  // Throws an error on encountering unknown flags
  const unknownFields = 0xffff
  expect(() => generalBitFlagsFromRaw(unknownFields)).toThrow(ZipUnsupportedError)
})

test('getUint64LEAsNumber()', () => {
  const ANSWER = 42
  // Make buffer slightly larger to test offset
  const buffer = new ArrayBuffer(10)
  const view = new DataView(buffer)
  const viewOffset = 2

  // Normal uint32
  view.setBigUint64(viewOffset, BigInt(ANSWER), true)
  expect(getUint64LEAsNumber(view, viewOffset)).toBe(ANSWER)

  // Integer exceeding uint32
  view.setBigUint64(viewOffset, BigInt(Number.MAX_SAFE_INTEGER), true)
  expect(getUint64LEAsNumber(view, viewOffset)).toBe(Number.MAX_SAFE_INTEGER)

  // Integer exceeding maximum safe value
  view.setBigUint64(viewOffset, BigInt(Number.MAX_SAFE_INTEGER) + BigInt(1), true)
  expect(() => getUint64LEAsNumber(view, viewOffset)).toThrow(ZipRangeError)
})
