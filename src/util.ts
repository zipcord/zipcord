/** Various utility functions used by the rest of the library  @packageDocumentation */

import { ZipInvalidError, ZipcordError } from './errors.js'


export const HTTP_PARTIAL_CONTENT = 206
export const HTTP_RANGE_NOT_SATISFIABLE = 416

/** Converts a `Blob` to an `ArrayBuffer`. Uses `Blob.ArrayBuffer` if
  available, otherwise falls back to a `FileReader`-based solution. Most modern
  browsers support `arrayBuffer`, however Safari as of v14 does not. */
export async function blobToArrayBuffer(blob: Blob): Promise<ArrayBuffer> {
  if (blob.arrayBuffer) {
    return blob.arrayBuffer()
  } else {
    const readPromise = new Promise<ArrayBuffer>( (resolve) => {
      const fileReader = new FileReader()
      fileReader.onloadend = () => {
        // result will be an ArrayBuffer
        resolve(fileReader.result as ArrayBuffer)
      }
      fileReader.readAsArrayBuffer(blob)
    })
    return readPromise
  }
}

/** Used by decodeBufferSlice */
const decoder = new TextDecoder()
/** Decode a section of an ArrayBuffer to a string. If offset omitted start at
 * 0, and if length omitted go to end of buffer */
export function decodeBufferSlice(buffer: ArrayBuffer, offset?: number, length?: number): string {
  const actualOffset = offset ?? 0
  const actualLength = length ?? buffer.byteLength - actualOffset
  return decoder.decode(buffer.slice(actualOffset, actualOffset + actualLength))
}

/** Writes the output of a `ReadableStream` into an `UnderlyingSink`.
  Used when host environment does not have WritableStream/pumpTo available. */
export async function pumpStreamIntoSink(
  stream: ReadableStream<Uint8Array>, sink: UnderlyingSink<Uint8Array>
): Promise<void> {
  const reader = stream.getReader()
  while (reader) {
    const {value} = await reader.read()
    // If undefined stream is closed
    if (value === undefined) {
      break
    } else {
      if (sink.write) {
        sink.write?.(
          new Uint8Array(value),
          { error: (e) => {throw new ZipInvalidError('corruptData', e)} }
        )
      } else {
        // But why would you create a sink for Zipcord without a write method?
        // Check for brain farts, I guess.
        throw new ZipcordError('UnderlyingSink for DecoderConfig must define write()')
      }
    }
  }
  sink.close?.()
}

/** Helper function for if a source needs to be type guarded */
export function sourceIsUrl(source: string | ArrayBuffer): source is string {
  return typeof source === 'string'
}
