/** Main Zipcord class definition  @packageDocumentation */

import { crc32 } from './crc32.js'
import { ZipEntry } from './entry.js'
import {
  ZipInvalidError, ZipRangeError, ZipReadError, ZipcordError,
  ZipUninitializedError, ZipUnsupportedError
} from './errors.js'
import { RangeFetcher, ALWAYS_FALLBACK } from './rangefetcher.js'
import {
  ZIP_CDENTRY_MIN_SIZE, ZIP_COMMENT_MAX_SIZE, ZIP_EOCD_MIN_SIZE,
  ZIP_EOCD_SIGNATURE, ZIP64_EOCD_LOCATOR_SIZE, ZIP64_EOCD_LOCATOR_SIGNATURE,
  ZIP64_EOCD_MIN_SIZE, ZIP64_EOCD_SIZE_MIN_VALUE, ZIP64_U32_PLACEHOLDER, LITTLE_ENDIAN,
  extraFieldMappingFromBuffer, generalBitFlagsFromRaw, getUint64LEAsNumber
} from './structs.js'
import { blobToArrayBuffer, decodeBufferSlice } from './util.js'

import type { ZipCDEntry, ZipEOCD, Zip64EOCD, Zip64EOCDLocator } from './structs.js'


/** Functions to initialize and create decoders to decompress deflated data.

  ** [[`createSink`]] is used to decode from a `ReadableStream`. Buffer
    decoding methods will usually call this method when possible.
  ** [[`decodeBuffer`]] is used when `ReadableStream` is not supported in the
    host environment or [[`disableInternalStreams`]] is `true`.
  ** [[`createTransformer`]] is used by the streaming methods (e.g.
    [[`bufferStream`]]) when `TransformStream` is available. Implementations
    may safely stub this method out if you are sure any code using it will
    never try to read an archive entry as a stream. Do this at your own risk,
    no promises will be made that Zipcord will not change this behavior in
    the future.
*/
export interface DecoderConfig {
  /** Factory function that creates an
    [`UnderlyingSink`](https://streams.spec.whatwg.org/#underlying-sink-api)
    object for decompressing deflated data. Sink will be used with a native
    [`WritableStream`](https://streams.spec.whatwg.org/#ws-model) if supported,
    but if not available Zipcord will manually pipe data to the sink to
    decode compressed data. This will result in a performance penalty over
    a native implementation of `pipeTo` but allows streaming decompression in
    any modern browser. If performance is an issue a page can check for the
    presence of `WriteableStream` & `pipeTo` and force buffer decoding (using
    `decodeBuffer`) with [`disableInternalStreams`].

    `createSink` is used when `ReadableStream` is supported in the host
    environment and [[`disableInternalStreams`]] is not set. If a streaming
    decoding method is called (e.g. [[`bufferStream`]]) and `TransformStream`
    is not supported, Zipcord will fall back to decoding to a buffer with
    `createSink` and returning the resulting output buffer as a `ReadableStream`.

    Errors during decoding are reported to Zipcord by throwing them from the
    sink. Do not reject the output promise or you will get hard-to-debug errors
    about unhandled rejections.

    @param uncompressedSize - Size of the decompressed file in bytes. May be
      ignored by the decoder.
    @returns Tuple containing the sink for `WritableStream` and a `Promise`
      that resolves to a buffer that contains the decompressed data when the
      stream is closed
  */
  createSink: (uncompressedSize: number) => [sink: UnderlyingSink<Uint8Array>, output: Promise<Uint8Array>]
  /** Factory function that creates a
    [`Transformer`](https://streams.spec.whatwg.org/#transformer-api) object
    for a [`TransformStream`](https://streams.spec.whatwg.org/#ts) for decoding
    a compressed stream to another stream.

    `createTransformer` is used when one of the streaming data methods are
    called (e.g. [[`textStream`]]) and TransformStream is supported on in the
    host environment.

    @param uncompressedSize - Size of the decompressed file in bytes. May be
      ignored by the decoder.
  */
  createTransformer: (uncompressedSize: number) => Transformer<Uint8Array, Uint8Array>
  /** Decodes an input buffer of raw DEFLATE data into a buffer of uncompressed
    data. Used when [[`disableInternalStreams`]] is set or `ReadableStream` is
    not available. May result in a performance benefit due to not requiring the
    overhead of streams, especially when the host environment does not support
    `pipeTo`.

    @param buffer - Input buffer to decode
    @param uncompressedSize - Size of the decompressed file in bytes. May be
      ignored by the decoder.
    @returns `Promise` resolving into a buffer containing the decoded data
   */
  decodeBuffer: (buffer: Uint8Array, uncompressedSize: number) => Promise<Uint8Array>
}

/** Configuration options for Zipcord's behavior.

  Zipcord should work out-of-the-box with default settings but it has some
  useful options. The [[`checkCRC32`]], [[`maxFallbackSize`]], or
  [[`rangeRequired`]] settings are what you're most likely interested in.

  All settings are optional, see each setting's documentation for details and
  defaults.
*/
export interface ZipcordOptions {
  /** If true Zipcord will check the CRC32 of a file entry after extracting
    it. Disabled by default for performance reasons, set to `true` if you want
    to verify extracted data. This setting is ignored when extracting to a
    `ReadableStream`.

    **Default*: `false`
  */
  checkCRC32?: boolean
  /** The EOCD at the end of the file is a variable-length structure that may
    exceed 64KB in size with a maximum-length archive comment. When opening an
    archive Zipcord will download a minimal amount of data (98 bytes) from
    the end of the file to parse the footer records. If an archive comment is
    present the EOCD may be larger than the amount downloaded and a second
    request to the server will be made to download the maximum size of the
    footers (~64KB). This setting is the amount of extra data that will be
    downloaded in the initial request to account for a possible archive comment.

    If you are sure the archive you are reading does not contain a comment, you
    can set this to 0 to minimize bandwidth usage. You can also set this to a
    larger value if you are reading archives with longer comments and want to
    avoid making a second 64KB request on opening them.

    The maximum length of a zip comment is [[`ZIP_COMMENT_MAX_SIZE`]] (65,535
    bytes). You can set this higher than that but you probably shouldn't.

    **Default*: `1024`
  */
  commentBufferSize?: number
  /** Deflate decoder initializer. Function that returns a `Promise` resolving
    to a [[`DecoderConfig`]]. It may also be an array of initializers, in which
    case Zipcord will call each function in order and use the first one that
    resolves. This allows you to set a fallback if one or more of them are
    unavailable.

    For example, you could set it to an array of two initializers, the first
    resolving to a WASM-based decoder and the second to one written in
    Javascript. If the first one rejects (e.g. because WASM isn't supported or
    there was a problem loading the .wasm module) Zipcord will then call the
    second initializer and use its result.

    The return value of the (successful) initializer is stored in
    [[`ZipcordRaw.decoder`]]. If all initalizers fail this will become a
    rejected promise with a [[`ZipcordError`]].

    When using [[`ZipcordRaw`]] this option must be set or an error will be
    thrown in the constructor. If you are using the default [[`Zipcord`]]
    export this option is not required and a default encoder is used. In this
    case if you set a decoder Zipcord will try it first and only fall back to
    the default decoder as a failsafe if all the decoders specified here reject.

    **Default*: &lt;No default in [[`ZipcordRaw`]], internal decoder in `Zipcord`&gt;
  */
  decoderSetup?: Array<() => Promise<DecoderConfig>> | (() => Promise<DecoderConfig>)
  /** Disables internal use of streams when decoding to a buffer or text. By
    default when `ReadableStream`/`WritableStream` is available Zipcord will
    use them to decode data as it is read. Setting this option to `true` forces
    Zipcord to download and decompress data as whole buffers instead. This
    may increase performance by eliminating overhead of streams but will
    increase memory requirements.

    Has no effect on [[`bufferStream`]]/[[`textStream`]] methods - this setting
    only changes how methods that decode entries to a single output buffer
    behave (e.g. [[`buffer`]]).

    **Default*: `false`
  */
  disableInternalStreams?: boolean
  /** Amount of extra data in bytes to download to account for extraFields in
    the local file header that are smaller or not present in the central
    directory. Increase if you are getting safety buffer errors with your zip
    archives when you extract a file entry.

    **Default*: `32`
  */
  localHeaderExtraBufferSize?: number
  /** Maximum size of an URL source Zipcord will download when HTTP `Range`
    requests are unsupported. Opening a file larger than this will throw a
    [[`ZipReadError`]] in this case. If this option is undefined (the default)
    Zipcord will download the entire archive no matter how large. Has no
    effect on URLs that support `Range` requests or buffer sources.

    If you are using this option the HTTP server MUST supply the
    `Content-Length` header if it does not support `Range` requests. If this
    header is not available (because the server did not provide it or CORS
    restrictions prevent access) opening an URL that does not support `Range`
    requests will throw a [[`ZipReadError`]].

    You can still force buffer reading using [[`disableUrlReading`]] when this
    option is set, regardless of archive size. This only affects automatic
    fallback when opening the archive.

    **Default*: Always use download fallback when possible (`undefined`)
  */
  maxFallbackSize?: number
  /** If true reading an URL source from a server that does not support HTTP
    `Range` requests will throw a [[`ZipReadError`]]. If false will download
    the entire archive and read it as a buffer in this case (see
    [[`maxFallbackSize`]] to limit the size of the archive it will download).
    Setting to true does not prevent switching to buffer reading e.g. by
    forcing it with [[`disableUrlReading`]], it only affects what happens when
    the server does not respect the Range header. Has no effect on buffer
    sources.

    **Default*: `false`
  */
  rangeRequired?: boolean
  /** @hidden Used to supply a default decoder to the base class. For
    internal use only, use [[decoderSetup]] to set a decoder. */
  _defaultDecoder?: () => Promise<DecoderConfig>
  /** @hidden decoderSetup normalized into an array */
  _decoderSetupArray?: Array<() => Promise<DecoderConfig>>
}
/** [[ZipcordOptions]] but every option is required */
type ZipcordOptionsFull = Required<Omit<ZipcordOptions, '_defaultDecoder'>>
const DEFAULT_ZIP_OPTIONS: ZipcordOptionsFull = {
  checkCRC32: false,
  commentBufferSize: 1024,
  decoderSetup: [],
  disableInternalStreams: false,
  localHeaderExtraBufferSize: 32,
  maxFallbackSize: ALWAYS_FALLBACK,
  rangeRequired: false,
  _decoderSetupArray: []
}
Object.freeze(DEFAULT_ZIP_OPTIONS)
Object.freeze(DEFAULT_ZIP_OPTIONS.decoderSetup)

/** Minimum amount of data to load for all footer structures */
const MIN_FOOTER_BUFFER_SIZE = ZIP64_EOCD_MIN_SIZE + ZIP64_EOCD_LOCATOR_SIZE +
  ZIP_EOCD_MIN_SIZE
/** Maximum amount of data to load when searching for EOCD record */
const MAX_FOOTER_BUFFER_SIZE = MIN_FOOTER_BUFFER_SIZE + ZIP_COMMENT_MAX_SIZE


/** Read a zip archive from an URL or buffer. Requires [[`decoderSetup`]] to be
  set. If you are not using your own decoder you should probably import
  [[`Zipcord`]] instead.

  zipcord-decoder-fflate adds \~14kb minified to the bundle size. If you are
  bringing your own decoder you can use this class to leave the built-in
  decoder out.
*/
export class ZipcordRaw {
  /** Resolves or rejects depending on status of decoder (e.g. a WASM decoder
    may reject if it can't load its `.wasm` module). Set to the result of the
    first successfully initialized decoder from
    [[`ZipcordOptions.decoderSetup`]]. If no initializers succeed it will
    reject with a [[`ZipcordError`]]. [[`ZipEntry`]] also checks this promise
    on extraction of a deflated entry and will throw the rejected error if it
    fails.
  */
  public readonly decoder: Promise<DecoderConfig>
  /** Zipcord configuration set when opened. Built from
    [[`ZipcordOptions`]], merging supplied settings with defaults. */
  public readonly options: ZipcordOptionsFull
  /** unchecked property for originalSource */
  #originalSource?: Parameters<typeof ZipcordRaw.prototype.initialize>[0]
  /** unchecked property for centralDirectory */
  #centralDirectory?: ZipCDEntry[]
  /** unchecked property for entries */
  #entries?: Map<string, ZipEntry>
  /** unchecked property for eocd */
  #eocd?: ZipEOCD
  /** unchecked property for offsetDelta */
  #offsetDelta?: number
  /** unchecked property for _rangeFetcher */
  #rangeFetcher?: RangeFetcher
  /** unchecked property for zip64EOCD */
  #zip64EOCD?: Zip64EOCD
  /** unchecked property for zip64EOCDLocator */
  #zip64EOCDLocator?: Zip64EOCDLocator

  /** Opens a zip archive from the given source. If it is a `string`, source is
    treated as an URL. Otherwise it is used as the zip file data. Options are
    passed to the `Zipcord` constructor.

    Zipcord will attempt to use HTTP `Range` requests to read an archive from
    an URL. When `Range` requests are unsupported by the server it will
    download the entire archive and behave as though it were reading from a
    buffer. If this behavior is undesired you can set the [[`rangeRequired`]]
    option and Zipcord will throw an error when opening an URL for a server
    that does not support the Range header. You can also set the option to
    limit the size of the archive it will download; archives exceeding this
    limit will throw an error on opening.

    The [[ZipcordOptions.maxFallbackSize|`maxFallbackSize`]] option will also
    throw an error if the server does not respond with either `Content-Length`
    or `Content-Range` headers. Use with care when reading from unknown URLs.

    @param source - Strings are treated as an URL to the archive. Otherwise, a
      `Blob`, `ArrayBuffer`, or `Uint8Array` or other ArrayBuffer view
      containing the archive file may be given.
    @param options - Options for this `Zipcord` instance. See
      [[`ZipcordOptions`]] for available options.

    @throws [[`ZipRangeError`]] when opening a Zip64 archive with values
      exceeding Zipcord's limits of 8PB
    @throws [[`ZipReadError`]] when a problem is encountered reading the archive:
      ** HTTP 4xx/5xx error or other issues with HTTP request
      ** [[`rangeRequired`]] is set but HTTP server does not support `Range` header
      ** [[ZipcordOptions.maxFallbackSize|`maxFallbackSize`]] is set, `Range`
        header is unsupported, and archive exceeds limit
      ** [[ZipcordOptions.maxFallbackSize|`maxFallbackSize`]] is set but
        server is not responding with `Content-Length` or `Content-Range` headers
      ** Archive exceeds Zipcord's file size limits (8 PB)
    @throws [[`ZipcordError`]] when no decoder is set in [[`decoderSetup`]]
  */
  public static async open(
    source: Parameters<typeof ZipcordRaw.prototype.initialize>[0],
    options?: ZipcordOptions
  ): Promise<ZipcordRaw> {
    const zip = new ZipcordRaw(options)
    await zip.initialize(source)
    return zip
  }

  /** Sets common properties. Must call [[`initialize`]] afterwards.

    You should prefer to call [[`open`]] instead to open an archive unless you
    have some reason to create the `Zipcord` instance before the archive is
    available.

    @throws [[`ZipcordError`]] when no decoder is set in [[`decoderSetup`]]
  */
  constructor(options: ZipcordOptions = {}) {
    // decoderSetup may be an array of setup functions, normalize into array
    const decoderSetupArray = []
    if (Array.isArray(options.decoderSetup)) {
      decoderSetupArray.push(...options.decoderSetup)
    } else if (typeof options.decoderSetup === 'function') {
      decoderSetupArray.push(options.decoderSetup)
    }
    if (options._defaultDecoder) {decoderSetupArray.push(options._defaultDecoder)}
    this.options = {
      ...DEFAULT_ZIP_OPTIONS,
      ...options,
      _decoderSetupArray: decoderSetupArray,
      // Zipcord shouldn't mutate options but prevent it just in case
      decoderSetup: []
    }

    if (this.options._decoderSetupArray.length === 0) {
      throw new ZipcordError('No decoders specified in options')
    }
    this.decoder = this.initializeDecoder()
  }

  /** Called after constructor to load needed information about the zip
    archive. Will download the EOCD and central directory if loading from an
    URL. Separate from constructor to allow async loading.

    You should prefer to use [[`open`]] unless you have some specific reason to
    create the Zipcord object before the source is available.
  */
  public async initialize(source: string | ArrayBufferLike | ArrayBufferView | Blob): Promise<void> {
    this.#originalSource = source

    let convertedSource: string | ArrayBuffer
    if (ArrayBuffer.isView(source)) {
      convertedSource = source.buffer
    } else if (globalThis.Blob && source instanceof Blob) {
      // Check with globalThis or it breaks in Node
      convertedSource = await blobToArrayBuffer(source)
    } else if (typeof source === 'string' || source instanceof ArrayBuffer || source instanceof SharedArrayBuffer) {
      convertedSource = source
    } else {
      throw new ZipUnsupportedError('environment', 'Unknown source type')
    }

    this.#rangeFetcher = new RangeFetcher(
      convertedSource, this.options.rangeRequired, this.options.maxFallbackSize
    )
    await this.loadZipInfo()
  }

  /** Called in constructor to initialize the decoder(s). Return value of this
    method is set to [[`decoder`]]. If all initializers fail promise rejects
    with a [[`ZipcordError`]]. */
  protected async initializeDecoder(): Promise<DecoderConfig> {
    let lastError
    for (const decoderInit of this.options._decoderSetupArray) {
      try {
        return await decoderInit()
      } catch (e) {
        lastError = e
        // Try the next decoder
        continue
      }
    }
    throw new ZipcordError(`All decoders failed to initialize (Last decoder error: ${lastError})`)
  }

  /** Downloads source and switches to buffer reading.

    Take care when using this with large archives, as it will download the
    entire file. You can check [[`filesize`]] before calling to make sure you
    won't be transfering an excessive amount of data.
   */
  public async disableUrlReading(): Promise<void> {
    return this._rangeFetcher.disableUrlReading()
  }

  /** Get data from end of file for loading footer structs

    @param requestedBufferSize - Desired size of buffer. Returned buffer may be
     smaller than this if file is smaller than requested size.

    @throws {ZipcordError} When unable to read buffer
  */
  protected async initializeFooterBuffer(requestedBufferSize: number): Promise<ArrayBuffer> {
    if (requestedBufferSize <= 0) {
      throw new ZipcordError(`requestedBufferSize must be greater than 0 (was ${requestedBufferSize})`)
    }

    let newBuffer
    try {
      newBuffer = await this._rangeFetcher.readArrayBuffer(-requestedBufferSize)
    } catch (e) {
      if (e instanceof ZipRangeError) {
        // ZipRangeError here caused by requestedBufferSize being larger than
        // the archive, try again with with the now known file size. Also
        // switch to buffer reading, as the entire file is being downloaded
        // anyways
        await this.disableUrlReading()
        newBuffer = await this._rangeFetcher.readArrayBuffer(0)
      } else if (e instanceof ZipUnsupportedError || e instanceof ZipReadError) {
        // No need to wrap error, throw as-is
        throw e
      } else {
        throw new ZipcordError('Could not initialize footer buffer: ' + e.message)
      }
    }

    return newBuffer
  }

  /** Get additional data from end of file for loading footer structs,
    prepending to an existing footer buffer

    @param requestedBufferSize - Desired size of buffer. Returned buffer may be
      smaller than this if file is smaller than requested size, or larger if
      previousBuffer was already larger.
    @param previousBuffer - Buffer containing data already loaded. Will
      download only the difference between new and previous and return
      concatenated data.
  */
  protected async growFooterBuffer(requestedBufferSize: number, previousBuffer: ArrayBuffer): Promise<ArrayBuffer> {
    if (requestedBufferSize <= 0) {
      throw new ZipcordError(`requestedBufferSize must be greater than 0 (was ${requestedBufferSize})`)
    }

    const actualBufferSize = Math.min(requestedBufferSize, this.filesize)
    if (actualBufferSize >= this.filesize) {
      // Disable HTTP streaming if buffer size would load entire file
      this._rangeFetcher.disableUrlReading()
    }

    // Download more data if needed to fill buffer
    let newBuffer
    if (previousBuffer) {
      if (actualBufferSize <= previousBuffer?.byteLength) {
        // previousBuffer already large enough, return as is
        newBuffer = previousBuffer
      } else {
        // Download difference between previous and requested buffer and
        // concatenate with existing buffer
        const fetchSize = actualBufferSize - previousBuffer.byteLength
        const partialBuffer = this._rangeFetcher.readArrayBuffer(this.filesize - actualBufferSize, fetchSize)
        const mergedArray = new Uint8Array(actualBufferSize)
        mergedArray.set(new Uint8Array(await partialBuffer))
        mergedArray.set(new Uint8Array(previousBuffer), fetchSize)
        newBuffer = mergedArray.buffer
      }
    } else {
      // No previousBuffer supplied, create new buffer
      newBuffer = await this._rangeFetcher.readArrayBuffer(this.filesize - actualBufferSize)
    }

    return newBuffer
  }

  /** Get size of central directory using value from Zip64 EOCD if present */
  protected getSizeofCD(): number {
    try {
      return this.zip64EOCD ? this.zip64EOCD.sizeofCD : this.eocd.sizeofCD
    } catch {
      // this.eocd accessor throws an error but this should be an internal
      // ZipcordError here
      throw new ZipcordError('getSizeofCD called before loading footers')
    }
  }

  /** Get offset of central directory using value from Zip64 EOCD if present */
  protected getOffsetCD(): number {
    try {
      return this.zip64EOCD ? this.zip64EOCD.offsetCD : this.eocd.offsetCD
    } catch {
      // this.eocd accessor throws an error but this should be an internal
      // ZipcordError here
      throw new ZipcordError('getOffsetCD called before loading footers')
    }
  }

  // Zip file structure handling
  ///////////////////////////////

  /** Loads EOCD record and central directory, and creates the file entries */
  protected async loadZipInfo(): Promise<void> {
    await this.loadFooter()
    if (this.eocd.entriesTotal !== this.eocd.entriesOnDisk) {
      throw new ZipUnsupportedError('multipart')
    }

    await this.loadCentralDirectory()

    // Build file listing
    this.#entries = new Map()
    for (const cdEntry of this.centralDirectory) {
      let actualFilename = cdEntry.filename
      const infozipUnicode = cdEntry.extraField.infozipUnicode
      if (infozipUnicode?.version === 1) {
        // Specs say to only use this field when the CRC-32 value matches the
        // CRC of the CD entry's filename
        if (crc32(new Uint8Array(cdEntry._filenameBuffer)) === infozipUnicode.entryNameCRC32) {
          actualFilename = infozipUnicode.filename
        }
      }
      const zipEntry = new ZipEntry(actualFilename, this, cdEntry)
      this.#entries.set(actualFilename, zipEntry)
    }
  }

  /** Gets the EOCD and Zip64 footers from the zip. To prevent downloading
    unnecessary data in the unlikely event of a large zip EOCD comment, will
    download a small buffer first before falling back to a maximum-size
    (~64kb) download.
  */
  protected async loadFooter(): Promise<void> {
    /* Load EOCD */
    // First search in a minimal buffer, if EOCD cannot be found (because of
    // zip file comments pushing the start of the EOCD out of the loaded
    // buffer) try again with the maximum buffer size
    let buffer = await this.initializeFooterBuffer(
      MIN_FOOTER_BUFFER_SIZE + this.options.commentBufferSize
    )
    let bufferOffset = this.filesize - buffer.byteLength
    let eocd
    try {
      eocd = this.findEOCD(buffer, this.filesize - buffer.byteLength)
    } catch (e) {
      // Possible for an initial search to find something that looks like an
      // EOCD in the comment but isn't - ignore invalid errors and hope the
      // second attempt at finding it works. Zip files are stupid.
      if (!(e instanceof ZipInvalidError)) {
        throw e
      }
    }
    if (!eocd) {
      buffer = await this.growFooterBuffer(
        MAX_FOOTER_BUFFER_SIZE + this.options.commentBufferSize, buffer
      )
      bufferOffset = this.filesize - buffer.byteLength
      eocd = this.findEOCD(buffer, bufferOffset)
      if (!eocd) {
        throw new ZipInvalidError('corruptArchive', 'Unable to find EOCD record')
      }
    }
    // EOCD loaded, remaining code searches for Zip64 footers
    this.#eocd = eocd

    /* Load Zip64 EOCD locator */
    const eocdSize = this.filesize - eocd._offset
    let bufferEOCDOffset = eocd._offset - bufferOffset
    // Return now if file too small to contain Zip64 footers
    if (this.filesize < eocdSize + ZIP64_EOCD_LOCATOR_SIZE + ZIP64_EOCD_MIN_SIZE) {
      return
    }
    // Increase footer buffer size to contain Zip64 footers if not large enough
    if (bufferEOCDOffset < ZIP64_EOCD_LOCATOR_SIZE) {
      // Also buffer the minimum Zip64 EOCD to minimize extra requests.
      // Technically the Zip64 EOCD could be 2^64 + ZIP64_EOCD_SIZE_MIN_VALUE
      // maximum bytes in size but AFAIK the Zip64 extensible data sector is
      // proprietary and unused so assume it's the minimum size for purposes of
      // prebuffering.
      buffer = await this.growFooterBuffer(
        (this.filesize - eocd._offset) + ZIP64_EOCD_LOCATOR_SIZE + ZIP64_EOCD_MIN_SIZE,
        buffer
      )
      bufferOffset = this.filesize - buffer.byteLength
      bufferEOCDOffset = eocd._offset - bufferOffset
    }
    const zip64Locator = this.loadZip64EOCDLocator(
      buffer,
      eocd._offset - bufferOffset - ZIP64_EOCD_LOCATOR_SIZE
    )
    // Return now if Zip64 EOCD locator not found
    if (!zip64Locator) {
      return
    }

    // Account for prepended data on Zip64 EOCD offset. Will probably fail on a
    // zip64 file that has data in the Zip64 EOCD extra field but AFAIK this
    // has never been used. Unfortunately the locator does not contain the size
    // of the Zip64 EOCD and there's no way to know it without for sure without
    // scanning the entire file (impractical in this library trying to minimize
    // data downloading). You can't use the offsets as given, because archives
    // with prepended data exist. You can't calculate based on CD offset+size
    // from the EOCD because those are placeholders in a Zip64 archive.
    // Compatibility with prepended archives was chosen over (probably non-
    // existant) Zip64 EOCD extra fields. Zip files are stupid.
    const prependDelta = (eocd._offset - ZIP64_EOCD_LOCATOR_SIZE - ZIP64_EOCD_MIN_SIZE) -
      zip64Locator.offsetZip64EOCD

    /* Load Zip64 EOCD */
    let zip64EOCDBuffer: ArrayBuffer
    let zip64EOCDBufferOffset: number
    if (bufferOffset > zip64Locator.offsetZip64EOCD + prependDelta) {
      // If Zip64 EOCD starts before existing buffer download new buffer at
      // correct offset
      zip64EOCDBuffer = await this._rangeFetcher.readArrayBuffer(
        zip64Locator.offsetZip64EOCD + prependDelta,
        zip64Locator._offset - (zip64Locator.offsetZip64EOCD + prependDelta)
      )
      zip64EOCDBufferOffset = 0
    } else {
      zip64EOCDBuffer = buffer
      zip64EOCDBufferOffset = zip64Locator.offsetZip64EOCD - bufferOffset + prependDelta
    }
    const zip64EOCD = this.parseZip64EOCD(zip64EOCDBuffer, zip64EOCDBufferOffset)
    this.#zip64EOCDLocator = zip64Locator
    this.#zip64EOCD = zip64EOCD
  }

  /** Locates and reads the EOCD from the ZIP's source

    @param buffer - `ArrayBuffer` to search for the EOCD
    @param bufferFileOffset - File offset of the start of the buffer (used to
     set offset in EOCD structure)
    @returns Parsed and validated EOCD record
  */
  protected findEOCD(buffer: ArrayBuffer, bufferFileOffset: number): ZipEOCD | undefined {
    const searchView = new DataView(buffer)
    let eocd
    let eocdOffset
    for (eocdOffset = buffer.byteLength - ZIP_EOCD_MIN_SIZE; eocdOffset >= 0; eocdOffset--) {
      if (searchView.getUint32(eocdOffset, LITTLE_ENDIAN) === ZIP_EOCD_SIGNATURE) {
        eocd = this.parseEOCD(buffer, bufferFileOffset, eocdOffset)
        // A zip comment can contain a valid EOCD signature, check that the rest
        // of the EOCD is also valid before accepting it. Zip files are stupid.
        if (this.validateEOCD(eocd)) {
          return eocd
        }
      }
    }
    // Result is undefined if EOCD could not be found
  }

  /** Loads the Zip64 EOCD locator record from given buffer at the offset.
   * Returns structure if present, otherwise undefined for not a Zip64 file.
   * Structure validation performed as part of load. */
  protected loadZip64EOCDLocator(buffer: ArrayBuffer, offset: number): Zip64EOCDLocator | undefined {
    const view = new DataView(buffer)
    // Signature must be checked before parsing, as the parser stores a 64-bit
    // value in a Number with 53-bit limit and will throw an error if the
    // record is not present in the file and whatever random value at
    // offsetZip64EOCD's location exceeds MAX_SAFE_INTEGER.
    if (view.getUint32(offset, LITTLE_ENDIAN) === ZIP64_EOCD_LOCATOR_SIGNATURE) {
      const zip64Locator = this.parseZip64EOCDLocator(buffer, this.filesize - buffer.byteLength, offset)
      // Locator signature could be part of valid non-zip64 data, validate
      // before handing it over
      if (this.validateZip64EOCDLocator(zip64Locator)) {
        return zip64Locator
      }
    }
    // If record was not found above return undefined
  }

  /** Downloads and parses the central directory of the zip file */
  protected async loadCentralDirectory(): Promise<void> {
    // Zip files may have data prepended to the archive (e.g. a SFX archive).
    // Calculate the difference between stored offsets and actual offsets.
    const zip64EOCDLength = this.zip64EOCD ? this.zip64EOCD.size + 12 : 0
    const zip64EOCDLocatorLength = this.zip64EOCDLocator ? ZIP64_EOCD_LOCATOR_SIZE : 0
    const zip64FooterLength = zip64EOCDLength + zip64EOCDLocatorLength
    const cdOffset = this.eocd._offset - this.getSizeofCD() - zip64FooterLength
    this.#offsetDelta = cdOffset - this.getOffsetCD()

    const buffer = await this._rangeFetcher.readArrayBuffer(cdOffset, this.eocd.sizeofCD)
    const entries: ZipCDEntry[] = []
    let nextEntryOffset = 0
    for (let entryIndex = 0; entryIndex < this.eocd.entriesTotal; entryIndex++) {
      const entry = this.parseCentralDirectoryRecord(buffer, nextEntryOffset)
      entries.push(entry)
      nextEntryOffset = nextEntryOffset + ZIP_CDENTRY_MIN_SIZE +
        entry.filenameLength + entry.extraFieldLength + entry.commentLength
    }

    this.#centralDirectory = entries
  }

  // File structure parsing
  //////////////////////////

  /** Converts an `ArrayBuffer` into an EOCD structure */
  protected parseEOCD(buffer: ArrayBuffer, bufferFileOffset: number, readOffset = 0): ZipEOCD {
    const view = new DataView(buffer, readOffset)
    // Buffer is known to be at least the size of a minimum EOCD by this point
    const commentLength = view.getUint16(20, LITTLE_ENDIAN)
    if (commentLength + ZIP_EOCD_MIN_SIZE > buffer.byteLength) {
      throw new ZipInvalidError(
        'corruptArchive', 'Zip comment length exceeds file length'
      )
    }

    return {
      signature: view.getUint32(0, LITTLE_ENDIAN),
      diskNum: view.getUint16(4, LITTLE_ENDIAN),
      diskWithCD: view.getUint16(6, LITTLE_ENDIAN),
      entriesOnDisk: view.getUint16(8, LITTLE_ENDIAN),
      entriesTotal: view.getUint16(10, LITTLE_ENDIAN),
      sizeofCD: view.getUint32(12, LITTLE_ENDIAN),
      offsetCD: view.getUint32(16, LITTLE_ENDIAN),
      commentLength,
      comment: decodeBufferSlice(buffer, readOffset + ZIP_EOCD_MIN_SIZE, commentLength),
      _offset: bufferFileOffset + readOffset
    }
  }

  /** Converts an `ArrayBuffer` into a Zip64 EOCD structure */
  protected parseZip64EOCD(buffer: ArrayBuffer, offset: number): Zip64EOCD {
    // It's theoretically possible offset could be outside the range of safe
    // integers for Number values but this library does not support 8PB+ zip
    // files. Why do you even HAVE zip files that large?
    if (!Number.isSafeInteger(offset)) {
      throw new ZipRangeError("File too large")
    }

    const view = new DataView(buffer, offset)
    const zip64EOCDSize = getUint64LEAsNumber(view, 4)
    return {
      signature: view.getUint32(0, LITTLE_ENDIAN),
      size: zip64EOCDSize,
      versionMadeBy: {
        host: view.getUint8(13),
        specification: view.getUint8(12),
        _raw: view.getUint16(12, LITTLE_ENDIAN)
      },
      versionExtract: view.getUint16(14, LITTLE_ENDIAN),
      diskNum: view.getUint32(16, LITTLE_ENDIAN),
      diskWithCD: view.getUint32(20, LITTLE_ENDIAN),
      entriesOnDisk: getUint64LEAsNumber(view, 24),
      entriesTotal: getUint64LEAsNumber(view, 32),
      sizeofCD: getUint64LEAsNumber(view, 40),
      offsetCD: getUint64LEAsNumber(view, 48),
      zip64ExtData: buffer.slice(
        offset + ZIP64_EOCD_MIN_SIZE,
        offset + ZIP64_EOCD_MIN_SIZE + (zip64EOCDSize - ZIP64_EOCD_SIZE_MIN_VALUE)
      ),
    }
  }

  /** Converts an `ArrayBuffer` into a Zip64 EOCD locator structure */
  protected parseZip64EOCDLocator(buffer: ArrayBuffer, bufferFileOffset: number, readOffset: number): Zip64EOCDLocator {
    const view = new DataView(buffer, readOffset)
    return {
      signature: view.getUint32(0, LITTLE_ENDIAN),
      diskWithZip64EOCD: view.getUint32(4, LITTLE_ENDIAN),
      offsetZip64EOCD: getUint64LEAsNumber(view, 8),
      diskTotal: view.getUint32(16, LITTLE_ENDIAN),
      _offset: bufferFileOffset + readOffset
    }
  }

  /** Parse a single record of the central directory */
  protected parseCentralDirectoryRecord(buffer: ArrayBuffer, offset: number): ZipCDEntry {
    const view = new DataView(buffer, offset)
    const filenameLength = view.getUint16(28, LITTLE_ENDIAN)
    const extraFieldLength = view.getUint16(30, LITTLE_ENDIAN)
    const commentLength = view.getUint16(32, LITTLE_ENDIAN)
    const filenameBuffer = buffer.slice(
      offset + ZIP_CDENTRY_MIN_SIZE, offset + ZIP_CDENTRY_MIN_SIZE + filenameLength
    )
    const extraField = extraFieldMappingFromBuffer(
      buffer, offset + ZIP_CDENTRY_MIN_SIZE + filenameLength, extraFieldLength
    )

    return {
      signature: view.getUint32(0, LITTLE_ENDIAN),
      versionMadeBy: {
        host: view.getUint8(5),
        specification: view.getUint8(4),
        _raw: view.getUint16(4, LITTLE_ENDIAN)
      },
      versionExtract: view.getUint16(6, LITTLE_ENDIAN),
      generalBitFlags: generalBitFlagsFromRaw(view.getUint16(8, LITTLE_ENDIAN)),
      compressionMethod: view.getUint16(10, LITTLE_ENDIAN),
      lastModTime: view.getUint16(12, LITTLE_ENDIAN),
      lastModDate: view.getUint16(14, LITTLE_ENDIAN),
      crc32: view.getUint32(16, LITTLE_ENDIAN),
      compressedSize: view.getUint32(20, LITTLE_ENDIAN),
      uncompressedSize: view.getUint32(24, LITTLE_ENDIAN),
      filenameLength,
      extraFieldLength,
      commentLength,
      diskNumberStart: view.getUint16(34, LITTLE_ENDIAN),
      internalFileAttributes: view.getUint16(36, LITTLE_ENDIAN),
      externalFileAttributes: view.getUint32(38, LITTLE_ENDIAN),
      localHeaderOffset: view.getUint32(42, LITTLE_ENDIAN),
      filename: decodeBufferSlice(
        buffer, offset + ZIP_CDENTRY_MIN_SIZE, filenameLength
      ),
      extraField,
      comment: decodeBufferSlice(
        buffer,
        offset + ZIP_CDENTRY_MIN_SIZE + filenameLength + extraFieldLength,
        commentLength
      ),
      _filenameBuffer: filenameBuffer
    }
  }

  // File structure validation
  /////////////////////////////

  /** Checks that EOCD is valid. EOCD file comments may contain a valid EOCD
    signature so check that other values are sensible as well. If you're
    working with zip files that have fully valid EOCDs in the EOCD comments
    then you probably have much bigger problems you should be fixing.
  */
  protected validateEOCD(eocd: ZipEOCD): boolean {
    const eocdSize = ZIP_EOCD_MIN_SIZE + eocd.commentLength
    return (
      // EOCD signature
      eocd.signature === ZIP_EOCD_SIGNATURE &&
      // Central directory must fit in available space
      eocd.sizeofCD <= this.filesize - eocdSize &&
      // Central directory must fit before EOCD start
      (
        eocd.offsetCD <= this.filesize - eocdSize - eocd.sizeofCD ||
        eocd.offsetCD === ZIP64_U32_PLACEHOLDER
      ) &&
      eocd.entriesOnDisk <= eocd.entriesTotal
      // Comment length will already always match because amount of data read
      // to assign to it is directly from EOCD record
    )
  }

  /** Checks that the Zip64 EOCD locator record is valid. Like the EOCD valid
    data may appear as part of a comment or other data, so also do a sanity
    check on the disk numbers.
  */
  protected validateZip64EOCDLocator(locator: Zip64EOCDLocator): boolean {
    return (
      locator.signature === ZIP64_EOCD_LOCATOR_SIGNATURE &&
      locator.diskWithZip64EOCD <= locator.diskTotal
    )
  }

  // Accessors
  /////////////

  /** Central directory records' contents */
  public get centralDirectory(): ZipCDEntry[] {
    if (!this.#centralDirectory) {
      throw new ZipUninitializedError()
    }

    return this.#centralDirectory
  }

  /** Archive comment, or undefined if no comment */
  public get comment(): string | undefined {
    if (this.eocd.commentLength > 0) {
      return this.eocd.comment
    } else {
      return
    }
  }

  /** `Map` containing [[`ZipEntry`]] objects keyed to entry filenames */
  public get entries(): Map<string, ZipEntry> {
    if (!this.#entries) {
      throw new ZipUninitializedError()
    }

    return this.#entries
  }

  /** The End Of Central Directory record at the end of the file */
  public get eocd(): ZipEOCD {
    if (!this.#eocd) {
      throw new ZipUninitializedError()
    }

    return this.#eocd
  }

  /** Array of all entry filenames */
  public get filenames(): string[] {
    // Will throw error from entires if not initialized
    return Array.from(this.entries.keys())
  }

  /** Size of the zip archive

    @throws [[`ZipUninitializedError`]] when source size unknown. Size should
      be available as soon as a successful request is completed, so this should
      only happen if you manually create a [[`ZipcordRaw`]] object without
      using [[`open`]] and try to access `filesize` before calling
      [[`initialize`]].
  */
  public get filesize(): number {
    if (this._rangeFetcher.sourceSize === undefined) {
      throw new ZipUninitializedError()
    }
    return this._rangeFetcher.sourceSize
  }

  /** Difference between stored offsets and actual offsets (typically caused by
    prepending data to the archive, e.g. an SFX extractor, without correcting
    stored offsets). */
  public get offsetDelta(): number {
    if (this.#offsetDelta === undefined) {
      throw new ZipUninitializedError()
    }

    return this.#offsetDelta
  }

  /** True if next read of archive data will be through an HTTP request.
    An archive opened from an URL may switch to reading from a buffer later,
    e.g. if the archive is small enough the initial parsing of footers reads in
    the entire file, or by manually switching to buffer reading using
    [[`disableUrlReading`]]. */
  public get readingFromUrl(): boolean {
    return this._rangeFetcher.readingFromUrl
  }

  /** Source of archive as provided to Zipcord */
  public get originalSource(): Parameters<typeof ZipcordRaw.prototype.initialize>[0] {
    if (!this.#originalSource) {
      throw new ZipUninitializedError()
    }

    return this.#originalSource
  }

  /** The Zip64 EOCD, or `undefined` if not present */
  public get zip64EOCD(): Zip64EOCD | undefined {
    if (!this.#eocd) {
      throw new ZipUninitializedError()
    }

    return this.#zip64EOCD
  }

  /** The Zip64 EOCD locator, or `undefined` if not present */
  public get zip64EOCDLocator(): Zip64EOCDLocator | undefined {
    if (!this.#eocd) {
      throw new ZipUninitializedError()
    }

    return this.#zip64EOCDLocator
  }

  /** Provide a way for [[`ZipEntry`]] objects to access archive's
    [[`RangeFetcher`]]

    @internal
    @hidden
   */
  public get _rangeFetcher(): RangeFetcher {
    if (!this.#rangeFetcher) {
      throw new ZipcordError('Zip source must be opened reading ranges')
    }
    return this.#rangeFetcher
  }
}
