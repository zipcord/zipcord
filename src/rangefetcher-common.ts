/** Exports a class that loads segments of data from a resource */

import { ZipRangeError, ZipReadError, ZipcordError, ZipUnsupportedError } from './errors.js'
import * as features from './features.js'
import { sourceIsUrl, HTTP_RANGE_NOT_SATISFIABLE, HTTP_PARTIAL_CONTENT } from './util.js'


/** Value of maxFallbackSize when no maximum size of source to download */
export const ALWAYS_FALLBACK = Number.MAX_SAFE_INTEGER

/** Perform a fetch() with a Range header. Will not throw errors on 4xx/5xx
  status codes, caller should treat it the same was as standard fetch.

  @param url - URL to fetch
  @param offset - Start of range to fetch, in bytes. Negative values offset
    from end of file.
  @param length - Amount of bytes to fetch. Ignored when offset is negative.
  @param options - Fetch options. Range header will be added/overwritten.
  @returns Promise resolving to response value of constructed fetch() call
*/
async function fetchRange(
  url: string, offset: number, length?: number, options: RequestInit = {}
): Promise<Response> {
  let byteRange
  if (offset < 0) {
    // Negative offsets in Range requests read from end of file
    byteRange = offset.toString()
  } else {
    // Specifically check that length is undefined. A length of 0 is valid but
    // evaluates to false. Why you would do this I don't know, but it's valid.
    const endByte = length !== undefined ? String(offset + length - 1) : ''
    byteRange = `${offset}-${endByte}`
  }

  // Range header will be overwritten if it was included in options
  const headers = {
    ...options.headers,
    Range: `bytes=${byteRange}`
  }
  return fetch(url, {
    ...options,
    headers
  })
}

/** Abstracts away reading ranges from URL or buffer. Can switch at any time
  from URL to buffer source if needed (e.g. file is too small to bother with
  fetch ranges).
*/
export class RangeFetcherBase {
  public headersLoaded = false
  public sourceSize?: number
  /** Source currently being used. May be different from source if initialized
   * with an URL but had URL streaming disabled later. */
  protected currentSource: string | ArrayBuffer
  #rangeSupported?: boolean

  /** Constructor for RangeFetcher
    @param source - Original source of data. If a string the URL of the
      resource, othersize a buffer containing the data.
    @param rangeRequired - If true do not allow downgrading to buffer - Range
      requests only
    @param maxFallbackSize - Maximum size of URL source to download for buffer
      fallback if Range not supported. If unset no limit is used.
   */
  constructor(
    /** Original source of data. If a string the URL of the resource, othersize
     * a buffer containing the data. */
    protected readonly source: string | ArrayBuffer,
    /** If true do not allow downgrading to buffer - Range requests only */
    protected readonly rangeRequired = false,
    /** Maximum size of URL source to download for buffer fallback if Range not
     * supported. If unset no limit is used. */
    protected readonly maxFallbackSize = ALWAYS_FALLBACK
  ) {
    this.currentSource = source
  }

  /** Downloads zip file and disables fetching from URL, switches to buffer reading */
  public async disableUrlReading(response?: Response): Promise<void> {
    if (sourceIsUrl(this.currentSource)) {
      const downloadResponse = response ?? await fetch(this.currentSource)
      // Changing currentSource from string will also make readingFromUrl
      // return false next time
      this.currentSource = await downloadResponse.arrayBuffer()
      this.setInfoFromArrayBuffer()
    }
  }

  /** Asyncronously get file information. If source is an URL results in an
    HTTP HEAD request for the URL. Not called at initialization to avoid
    making unneccessary requests.

    @throws {ZipUnsupportedError} when Content-Length or Content-Range headers
      not present in HTTP response
  */
  public async loadSourceHeaders(): Promise<void> {
    if (sourceIsUrl(this.currentSource)) {
      const response = await fetch(this.currentSource, {method: 'HEAD'})
      this.setInfoFromResponseHeaders(response.headers)
    } else {
      this.setInfoFromArrayBuffer()
    }

    this.headersLoaded = true
  }

  /** Return a given range as an ArrayBuffer. If length not given will return
    all data after offset. Negative offset is from end of file, length is
    ignored in this case.

    @param offset - Offset of start of range. Negative offset reads from end
      of file.
    @param length - Amount of data to read in bytes. Reads to end if not
      given. Ignored when offset is negative.
    @returns Promise resolving to an ArrayBuffer containing the requested data.

    @throws {ZipRangeError} when range is invalid. If source size is unknown may
      make an HTTP request before throwing.
    @throws {ZipArchiveTooBigError} when HTTP server does not support Range
      requests and source is larger than maxFallbackSize
    @throws {ZipUnsupportedError} when HTTP server does not respond with a
      Content-Length or Content-Range header in response
    @throws {ZipReadError} when a 4xx/5xx response is recieved reading from an URL
  */
  public async readArrayBuffer(offset: number, length?: number): Promise<ArrayBuffer> {
    // If sourceSize was not set yet will check this again after getting data
    if (this.sourceSize !== undefined) {
      this.checkRangeBounds(offset, length)
    }

    let buffer: Promise<ArrayBuffer>
    if (sourceIsUrl(this.currentSource)) {
      const response = await this.getFetchRangeResponse(offset, length)
      if (response.status === HTTP_PARTIAL_CONTENT) {
        // Response is a successful range request
        buffer = response.arrayBuffer()
      } else {
        // Response was successful but not a range, switch to buffer reading
        await this.disableUrlReading(response)
        buffer = Promise.resolve(this.getSourceBufferRange(offset, length))
      }
    } else {
      // Buffer source
      buffer = Promise.resolve(this.getSourceBufferRange(offset, length))
    }

    return buffer
  }

  /** Return a given range as a ReadableStream. If length not given will return
    all data after offset.

    URL sources can be streamed if Response.prototype.body is available. Buffer
    sources may be streamed if Blob.protoype.stream() is present. Will throw
    an error if streaming not supported, check with streamingEnabled first.
  */
  public async readStream(offset: number, length?: number): Promise<ReadableStream<Uint8Array>> {
    // If sourceSize was not set yet will check this again after getting data
    if (this.sourceSize !== undefined) {
      this.checkRangeBounds(offset, length)
    }

    let reader: ReadableStream
    if (sourceIsUrl(this.currentSource)) {
      const response = await this.getFetchRangeResponse(offset, length, true)
      if (response.status === HTTP_PARTIAL_CONTENT) {
        // Response is a successful range request
        // Check both body and getReader to account for Node
        if (response.body?.getReader) {
          reader = response.body
        } else {
          // Either Response.body or Blob.stream() known to be available from
          // requireStream check in getFetchRangeResponse
          const blob = await response.blob()
          reader = blob.stream()
        }
      } else {
        // Response was successful but not a range, switch to buffer reading
        await this.disableUrlReading(response)
        reader = await this.readStream(offset, length)
      }
    } else {  // Buffer source
      if (features.BLOB_STREAM) {
        const blobRange = new Blob([this.getSourceBufferRange(offset, length)])
        reader = blobRange.stream()
      } else {
        throw new ZipUnsupportedError('environment', 'Blob.stream() not available')
      }
    }

    return reader
  }

  /** Check that a range is valid for the source. Range errors here are a logic
    error in the library and so throws an error.

    Needs size of source and will throw error if called before sourceSize is
    available.

    @throws {ZipRangeError} when range outsize of source bounds
  */
  protected checkRangeBounds(offset: number, length?: number): void {
    if (this.sourceSize === undefined) {
      throw new ZipcordError('Checked range bounds against unknown source size')
    }

    // End offset is exclusive
    const end = length === undefined ? this.sourceSize : offset + length
    if (offset < 0 && -offset > this.sourceSize) {
      throw new ZipRangeError(`Reading before start of file (offset ${offset}, sourceSize ${this.sourceSize})`)
    } else if (end > this.sourceSize) {
      throw new ZipRangeError(
        `Reading past end of file (Reading range ${offset}-${end} ` +
        `${length ?? this.sourceSize}bytes, File size: ${this.sourceSize})`)
    }
  }

  /** Perform a fetch request and check response for errors before returning it.

    @param offset - Offset of start of range. Negative offset reads from end
      of file.
    @param length - Amount of data to read in bytes. Reads to end if not
      given. Ignored when offset is negative.
    @param requireStream - Response.body or Blob.stream must be available, or
      fetch is aborted and ZipUnsupportedError is thrown.
    @returns Promise resolving to a Response returned from Fetch

    @throws {ZipcordError} when this.currentSource is not an URL
    @throws {ZipRangeError} when range is invalid. If source size is unknown may
      make an HTTP request before throwing.
    @throws {ZipArchiveTooBigError} when HTTP server does not support Range
      requests and source is larger than maxFallbackSize
    @throws {ZipUnsupportedError} when HTTP server does not respond with a
      Content-Length or Content-Range header in response, or when requireStream
      is set but no streaming methods available
    @throws {ZipReadError} when a 4xx/5xx response is recieved reading from an URL
   */
  protected async getFetchRangeResponse(offset: number, length?: number, requireStream = false): Promise<Response> {
    if (!sourceIsUrl(this.currentSource)) {
      throw new ZipcordError('Tried to get fetch range from Blob source')
    }

    const abortController = new AbortController()
    const response = await fetchRange(
      this.currentSource,
      offset,
      length,
      {signal: abortController.signal}
    )
    if (this.sourceSize === undefined) {
      try {
        this.setInfoFromResponseHeaders(response.headers)
      } catch (e) {
        // Cancel download if problem setting info
        abortController.abort()
        throw e
      }
      // sourceSize may still be undefined if Content-Range/Length not in headers
      if (this.sourceSize !== undefined) {
        this.checkRangeBounds(offset, length)
      }
    }

    // Check response problems
    if (response.status === HTTP_RANGE_NOT_SATISFIABLE) {
      // Requested range outside of file bounds on server.
      throw new ZipRangeError(`416 Range not Satisfiable recieved from server (Offset ${offset}, length ${length})`)
    } else if (response.status >= 400) {
      // HTTP error reading URL (except 416 handled above)
      throw new ZipReadError('httpStatus', response)
    } else if ((response.body === undefined && !features.BLOB_STREAM) && requireStream) {
      // A method to stream the response data must be available
      abortController.abort()
      throw new ZipUnsupportedError('environment', 'Response.body or Blob.stream() not available')
    } else if (response.status !== HTTP_PARTIAL_CONTENT) {
      // If server is not responding with range headers download the file and
      // switch to buffer reading - unless rangeRequired is set, then that's an
      // error for the caller to handle
      if (this.rangeRequired) {
        abortController.abort()
        throw new ZipReadError('rangeRequired')
      }
      // Do not switch to buffer reading if archive's size too large
      if (this.sourceSize && this.sourceSize > this.maxFallbackSize) {
        abortController.abort()
        throw new ZipReadError('tooBig', this.sourceSize, this.maxFallbackSize)
      }
      // Source size is required to be available when a maximum fallback size is set
      if (this.sourceSize === undefined && this.maxFallbackSize !== ALWAYS_FALLBACK) {
        abortController.abort()
        throw new ZipReadError('contentSize')
      } else {
        // No problems with response
      }
    }

    return response
  }

  /** Gets a slice of the current source buffer.

    @param offset - Offset of start of range. Negative offset reads from end
      of file.
    @param length - Amount of data to read in bytes. Reads to end if not
      given. Ignored when offset is negative.
    @returns ArrayBuffer containing the given range from the source buffer

    @throws {ZipcordError} when this.currentSource is not a buffer
    @throws {ZipRangeError} when range is invalid
  */
  protected getSourceBufferRange(offset: number, length?: number): ArrayBuffer {
    if (sourceIsUrl(this.currentSource)) {
      throw new ZipcordError('Tried to get ArrayBuffer range from URL source')
    }

    if (this.sourceSize === undefined) {
      this.setInfoFromArrayBuffer()
      this.checkRangeBounds(offset, length)
    }
    const endByte = length !== undefined ? offset + length : undefined
    return this.currentSource.slice(offset, endByte)
  }

  /** Sets sourceSize based on buffer source size */
  protected setInfoFromArrayBuffer(): void {
    this.#rangeSupported = false

    if (!sourceIsUrl(this.currentSource)) {
      this.sourceSize = this.currentSource.byteLength
    } else {
      throw new ZipcordError('Cannot set size from buffer when URL is source')
    }
  }

  /** Sets instance attributes based on headers from a fetch Response object */
  protected setInfoFromResponseHeaders(headers: Headers): void {
    // Range requests supported
    this.#rangeSupported = headers.get('Accept-Ranges') === 'bytes'

    // File size
    const rangeHeader = headers.get('Content-Range')
    const lengthHeader = headers.get('Content-Length')
    if (rangeHeader) {
      const size = Number(rangeHeader.split('/')[1])
      if (isNaN(size)) {
        throw new ZipcordError('Could not parse Content-Range header')
      }
      this.sourceSize = size
    } else if (lengthHeader) {
      const size = Number(lengthHeader)
      if (isNaN(size)) {
        throw new ZipcordError('Could not parse Content-Length header')
      }
      this.sourceSize = size
    }

    // this.sourceSize may still be unset at this point. If Content-Range is
    // not available disableUrlReading() will be called, sourceSize is set there.
  }

  // Properties
  //////////////

  /** True if source URL supports Range requests, false if not or reading from
   * buffer. Throws an error if accessed before loading headers. */
  public get rangeSupported(): boolean {
    if (this.#rangeSupported === undefined) {
      throw new ZipcordError('RangeReader not initialized')
    }
    return this.#rangeSupported
  }

  /** True if currently reading from an URL with range requests, false if
   * reading from a buffer */
  public get readingFromUrl(): boolean {
    return sourceIsUrl(this.currentSource)
  }

  /** True if ReadableStream available for current source, otherwise false. Use
    before calling readStream() to check if allowed.
  */
  public get streamingEnabled(): boolean {
    if (this.readingFromUrl && features.RESPONSE_STREAM) {
      return true
    } else if (!this.readingFromUrl && features.BLOB_STREAM) {
      return true
    } else {
      return false
    }
  }
}
