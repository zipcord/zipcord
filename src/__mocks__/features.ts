// Disable all browser-specific streaming features. Can remove these mocks if
// unit testing is performed in the browser in the future.

export const BLOB_STREAM = false
/** ReadableStream.pipeThrough() */
export const PIPE_THROUGH = false
/** ReadableStream.pipeTo() */
export const PIPE_TO = false
/** Response.body */
export const RESPONSE_STREAM = false
/** TransformStream */
export const TRANSFORM_STREAM = false
/** WritableStream */
export const WRITABLE_STREAM = false

/** ReadableStream.pipeThrough() and TransformStream */
export const PIPE_THROUGH_TRANSFORM = PIPE_THROUGH && TRANSFORM_STREAM
/** ReadableStream.pipeTo() and WritableStream */
export const PIPE_TO_WRITABLE = PIPE_TO && WRITABLE_STREAM
