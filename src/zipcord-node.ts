/** Zipcord using the Node's zlib to decode by default.

The *Stream methods on [`ZipEntry`] are not supported under Node, as it does
not support [Javascript Streams](https://streams.spec.whatwg.org/). They may
function with a polyfill, however this is untested and unsupported. As an
alternative when used in Node ZipENtry provides two additional methods
[[`bufferStreamNode`]] and [[`textStreamNode`]] that allow reading the file as
a [Node `Readable` stream](https://nodejs.org/api/stream.html#stream_readable_streams).

@packageDocumentation
*/

import * as zlib from 'zlib'

import { ZipcordRaw } from './zipcord-raw.js'

import type { DecoderConfig, ZipcordOptions } from './zipcord-raw.js'


/** Setup function for using Node zlib with [[`decoderSetup`]] */
export function nodeConfigSetup(): Promise<DecoderConfig> {
  return Promise.resolve({
    createSink: () => {
      /** Output chunks are stored here and merged on closing stream */
      const outChunks: Array<Buffer> = []
      let bytesWritten = 0
      /** zlib errors set here */
      let error: Error
      /** Called to resolve buffer promise on closing stream */
      let promiseResolve: (buffer: Uint8Array) => void
      /** Promise that resolves to the output buffer */
      const outputPromise = new Promise<Uint8Array>(
        (res) => { promiseResolve = res }
      )

      /** Node zlib inflateRaw stream */
      const zlibStream = zlib.createInflateRaw()
      zlibStream.on('data', (outChunk: Buffer) => {
        outChunks.push(outChunk)
        bytesWritten += outChunk.length
      })
      zlibStream.on('error', (e) => { error = e })

      const sink: UnderlyingSink<Uint8Array> = {
        write(chunk) {
          // Output chunks stored in data handler above
          zlibStream.write(chunk)
        },
        async close() {
          zlibStream.end()
          // Wait for stream to flush
          await new Promise( (res) => zlibStream.on('close', res))
          if (error) {
            throw error
          }

          if (outChunks.length === 0) {
            // No chunks were written, should not happen normally
            throw new Error('No chunks written to decoder before closing')
          } else if (outChunks.length === 1) {
            // If the output is a single chunk just return it directly
            promiseResolve(outChunks[0])
          } else {
            // Multiple output chunks to be combined
            const concatBuffer = new Uint8Array(bytesWritten)
            let offset = 0
            for (const chunk of outChunks) {
              chunk.copy(concatBuffer, offset)
              offset += chunk.length
            }
            promiseResolve(concatBuffer)
          }
        }
      }
      return [sink, outputPromise]
    },
    createTransformer: () => { throw new Error('TransformStream not supported in Node decoder') },
    decodeBuffer: async (buffer: Uint8Array) => { return zlib.inflateRawSync(buffer) }
  })
}

/** Zipcord with a default Node.js [zlib](https://nodejs.org/api/zlib.html)-based
  decoder. Exported as [[`Zipcord`]] and the default export of the package on
  Node. See [[`ZipcordRaw`]] for full documentation on usage.

  Node does not support [Javascript Streams](https://streams.spec.whatwg.org/)
  and using any of the *Stream methods will throw an error. It may be possible
  to polyfill streams however this is untested and unsupported.
*/
export class ZipcordNode extends ZipcordRaw {
  /** Opens a zip archive with a default setting for [[`decoder`]]. See
    [[`ZipcordRaw.open`]] for full documentation on usage. */
  public static async open(source: Parameters<typeof ZipcordRaw.open>[0], options?: ZipcordOptions): Promise<ZipcordRaw> {
    return ZipcordRaw.open(source, {...options, _defaultDecoder: nodeConfigSetup})
  }

  /** Calls parent constructor with a default setting for [[`decoder`]] */
  constructor(options: ZipcordOptions) {
    super({...options, _defaultDecoder: nodeConfigSetup})
  }
}
