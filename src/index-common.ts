/** Exports common to both web and Node targets

  @packageDocumentation
*/

export { ZipEntry } from './entry.js'
export * as errors from './errors.js'
export {
  CompressionMethod, ExtraFieldIds, VersionHost,
  ZIP_COMMENT_MAX_SIZE
} from './structs.js'
export { ZipcordRaw } from './zipcord-raw.js'

export type { DecoderConfig, ZipcordOptions } from './zipcord-raw.js'
export type {
  ExtraFieldInfozipUnicode, ExtraFieldZip64,
  ZipCDEntry, ZipEOCD, ZipExtraFieldMapping, ZipGeneralFlags,
  ZipLocalFileHeader, ZipVersionMadeBy, Zip64EOCD, Zip64EOCDLocator
} from './structs.js'
