import { ZipcordNode } from './zipcord-node'

import { getEntryOrFail, getFile, COMPRESSION_TEXT, HELLO_FILENAME, HELLO_TEXT } from './testdata'


test('Extract a stored file with ZipcordNode', async () => {
  const zip = await ZipcordNode.open(getFile('all.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
})

test('Extract a deflated file with ZipcordNode', async () => {
  const zip = await ZipcordNode.open(getFile('deflate.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(COMPRESSION_TEXT)
})

test('Extract a stored archive entry as a Node stream', async () => {
  const zip = await ZipcordNode.open(getFile('all.zip'))
  const stream = await getEntryOrFail(zip, HELLO_FILENAME).textStreamNode()
  let output = ''
  const streamPromise = new Promise<string>((res, rej) => {
    stream.on('data', (chunk) => output += chunk)
    stream.on('end', () => res(output))
    stream.on('error', (e) => rej(e))
  })
  expect(await streamPromise).toEqual(HELLO_TEXT)
})

test('Extract a deflated archive entry as a Node stream', async () => {
  const zip = await ZipcordNode.open(getFile('deflate.zip'))
  const stream = await getEntryOrFail(zip, HELLO_FILENAME).textStreamNode()
  let output = ''
  const streamPromise = new Promise<string>((res, rej) => {
    stream.on('data', (chunk) => output += chunk)
    stream.on('end', () => res(output))
    stream.on('error', (e) => rej(e))
  })
  expect(await streamPromise).toEqual(COMPRESSION_TEXT)
})
