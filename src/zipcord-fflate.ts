/** Zipcord using zipcord-decoder-fflate by default  @packageDocumentation */

import { fflateDecoderSetup } from 'zipcord-decoder-fflate'
import { ZipcordRaw } from './zipcord-raw.js'

import type { ZipcordOptions } from './zipcord-raw.js'

export { fflateDecoderSetup } from 'zipcord-decoder-fflate'

/** Zipcord using
  [zipcord-decoder-fflate](https://gitlab.com/zipcord/zipcord-decoder-fflate)
  as the default decoder. Exported as [[`Zipcord`]] and as the default export
  of the package on browser targets. See [[`ZipcordRaw`]] for full
  documentation on usage.
*/
export class ZipcordFflate extends ZipcordRaw {
  /** Opens a zip archive with a default setting for [[`decoder`]]. See
    [[`ZipcordRaw.open`]] for full documentation on usage. */
  public static async open(source: string | ArrayBufferLike | Blob, options?: ZipcordOptions): Promise<ZipcordRaw> {
    return ZipcordRaw.open(source, {...options, _defaultDecoder: fflateDecoderSetup})
  }

  /** Calls parent constructor with a default setting for [[`decoder`]] */
  constructor(options: ZipcordOptions) {
    super({...options, _defaultDecoder: fflateDecoderSetup})
  }
}
