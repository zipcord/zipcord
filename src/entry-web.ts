/** ZipEntry implementation for web targets

  @packageDocumentation
*/

export { ZipEntryBase as ZipEntry } from './entry-common.js'
