import { TextDecoder } from 'util'

import { Response } from 'node-fetch'


// eslint-disable-next-line @typescript-eslint/no-explicit-any
const globalAny = global as any
globalAny.TextDecoder = TextDecoder
globalAny.Response = Response
