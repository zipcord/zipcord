/**  Sets up data used by tests */

import { createHash } from 'crypto'
import * as fs from 'fs'
import * as path from 'path'

import * as fetchMock from 'fetch-mock'

import { HTTP_PARTIAL_CONTENT, HTTP_RANGE_NOT_SATISFIABLE } from '../util'

import type { ZipEntry } from '../entry'
import type { DecoderConfig, ZipcordRaw } from '../zipcord-raw'


// File loading
////////////////

/** Nonfunctional placeholder decoder used in decoder setup tests */
export const DUMMY_DECODER: DecoderConfig = {
  createSink: () => [ {}, Promise.resolve(new Uint8Array()) ],
  createTransformer: () => ({}),
  decodeBuffer: () => Promise.resolve(new Uint8Array())
}

/** All valid files that can be used for testing */
type ValidFilenames =
  // Zip examples
  'all.zip' | 'all_appended_bytes.zip' | 'all_prepended_bytes.zip' |
  'archive_comment.zip' | 'data_descriptor.zip' | 'deflate.zip' |
  'empty.zip' | 'extra_attributes.zip' | 'subfolder.zip' |
  'all-stream.zip' | 'deflate-stream.zip' |
  // Text encoding
  'local_encoding_in_name.zip' | 'pile_of_poo.zip' | 'utf8_in_name.zip' |
  // Zip64
  'zip64.zip' | 'zip64_appended_bytes.zip' | 'zip64_prepended_bytes.zip' |
  // Special cases
  'infozip_unicode_path.zip' | 'slashes_and_izarc.zip' |
  // Invalid zip files
  'invalid/bad_decompressed_size.zip' | 'invalid/bad_offset.zip' |
  'invalid/compression.zip' | 'invalid/crc32.zip' | 'invalid/encrypted.zip' |
  'invalid/all_missing_bytes.zip' | 'invalid/zip64_missing_bytes.zip'

/** Based directory containing test files */
const zipBasePath = path.join(__dirname, 'files')

/** Reads a sample file into a buffer */
export function getFile(filename: ValidFilenames): ArrayBuffer {
  const nodeBuffer = fs.readFileSync(path.join(zipBasePath, filename))
  return nodeBuffer.buffer.slice(nodeBuffer.byteOffset, nodeBuffer.byteOffset + nodeBuffer.byteLength)
}

/** Loads a sample of raw deflated data from deflate.zip */
export async function getCompressedSample(): Promise<ArrayBuffer> {
  const archive = getFile('deflate.zip')
  // Values are offsets of the deflated compressed data of entry inside file
  return archive.slice(39, 39 + 73)
}

/** Calculate the SHA-1 of an ArrayBuffer */
export function arrayBufferSha1(buffer: ArrayBuffer): string {
  return createHash('sha1').update(new DataView(buffer)).digest('hex')
}

/** Retrieves a zip entry from the given zip or fail the test if not present */
export function getEntryOrFail(zip: ZipcordRaw, filename: string): ZipEntry {
  const entry = zip.entries.get(filename)
  if (!entry) {
    throw new Error(`File entry "${filename}" not found`)
  }
  return entry
}


// Data about test file contents
/////////////////////////////////
export const COMPRESSION_TEXT = 'This a looong file : we need to see the difference between the different compression methods.\n'
export const HELLO_FILENAME = 'Hello.txt'
export const HELLO_TEXT = 'Hello World\n'
export const LOCAL_DIRPATH = '\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD \uFFFD\uFFFD\uFFFD\uFFFD\uFFFD/'
export const LOCAL_FILEPATH = '\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD \uFFFD\uFFFD\uFFFD\uFFFD\uFFFD/\uFFFD\uFFFD\uFFFD\uFFFD \u296A\uFFFD\u2BA2\uFFFD \uFFFD\uFFFD\uFFFD\u3B25\uFFFD\uFFFD.txt'
export const SAMPLE_UNCOMPRESSED_SIZE = 94
export const SMILE_FILENAME = 'images/smile.gif'
export const SMILE_SHA1 = 'e34cbfb344ad3811cc54a0909190afba946469b0'


// URL mocking
///////////////

const commonHeaders = {'Accept-Ranges': 'bytes'}
// NOTE: Will need to adjust regex if suffix-length syntax implemented
const reRangeHeaderBytes = /bytes=(\d*)-(\d*)/
/** Create a function for fetchMock's response function, that handles range
 * requests with the given source data.
 *
 * Does not implement full range header specs, only the functionality used by
 * Zipcord. In particular does not support multiple ranges or non-byte units.
 *
 * @param sourceData - String or buffer to return on fetch
 * @returns Will return range subset of data if a Range header is present in
 * the request, otherwise returns all data
 * @throws Will throw an error on a malformed/unsupported Range header
 */
function createFetchMockRangeHandler(sourceData: string | ArrayBuffer): fetchMock.MockResponseFunction {
  const sourceSize = typeof sourceData === 'string' ? sourceData.length : sourceData.byteLength
  return (_, options) => {
    const rangeHeader = Object.getOwnPropertyDescriptor(
      options?.headers ?? {}, 'Range')?.value as string

    if (rangeHeader) {
      const headerMatches = rangeHeader.match(reRangeHeaderBytes)
      if (headerMatches && headerMatches.length === 3) {
        let start, end
        const startMatch = headerMatches[1]
        const endMatch = headerMatches[2]
        if (startMatch !== '') {
          start = Number(startMatch)
          end = endMatch !== '' ? Number(endMatch) : sourceSize - 1
        } else {
          if (endMatch === '') {
            throw new Error('Range invalid, no values set')
          }
          start = sourceSize - Number(endMatch)
          end = sourceSize - 1
        }

        if (start < 0 || end >= sourceSize) {
          return {
            status: HTTP_RANGE_NOT_SATISFIABLE,
            headers: {
              ...commonHeaders,
              'Content-Range': `bytes */${sourceSize}`
            },
          }
        }
        const body = typeof sourceData === 'string' ?
          sourceData.substr(start, end - start + 1) :
          sourceData.slice(start, end + 1)
        return {
          status: HTTP_PARTIAL_CONTENT,
          headers: {
            ...commonHeaders,
            'Content-Range': `bytes ${start}-${end}/${sourceSize}`
          },
          body
        }
      } else {
        throw new Error(`Unable to parse Range header "${rangeHeader}"`)
      }
    } else {
      // No range header = return all data as normal
      return {
        headers: {...commonHeaders, 'Content-Length': sourceSize},
        body: 'sourceData'
      }
    }
  }
}


// Set up fetch mocks
/** URL supporting range requests */
export const MOCK_FETCHRANGE_URL = 'fetchrange'
export const MOCK_FETCHRANGE_DATA = '0123456789'
/** URL that responds with 404 Not Found */
export const MOCK_HTTP_404_URL = 'http404'
/** URL that responds with 500 Internal Server Error */
export const MOCK_HTTP_500_URL = 'http500'
/** URL that does not support Ranges or respond with a Content-Length header */
export const MOCK_NOLENGTH_URL = 'nolength'
export const MOCK_NOLENGTH_DATA = MOCK_FETCHRANGE_DATA
/** URL without support for range requests */
export const MOCK_NORANGE_URL = 'norange'
export const MOCK_NORANGE_DATA = MOCK_FETCHRANGE_DATA
/** URL of a valid zip archive */
export const MOCK_ZIPFILE_URL = 'all.zip'
/** URL of a valid zip archive that does not support HTTP Range requests */
export const MOCK_ZIPFILE_NORANGE_URL = 'all.zip?norange=true'
/** URL of a valid zip archive that does not support HTTP Range requests or
 * provide a Content-Length header */
export const MOCK_ZIPFILE_NOLENGTH_URL = 'all.zip?nolength=true'

const zipBuffer = getFile('all.zip')

fetchMock.config.sendAsJson = false

fetchMock.mock(MOCK_FETCHRANGE_URL, createFetchMockRangeHandler(MOCK_FETCHRANGE_DATA))
fetchMock.mock(MOCK_HTTP_404_URL, {status: 404})
fetchMock.mock(MOCK_HTTP_500_URL, {status: 500})
fetchMock.mock(MOCK_NOLENGTH_URL, MOCK_NOLENGTH_DATA, {includeContentLength: false})
fetchMock.mock(MOCK_NORANGE_URL, MOCK_NORANGE_DATA)
fetchMock.mock(MOCK_ZIPFILE_URL, createFetchMockRangeHandler(zipBuffer))
fetchMock.mock(MOCK_ZIPFILE_NORANGE_URL, {
  body: zipBuffer, headers: {'Content-Length': zipBuffer.byteLength}
})
fetchMock.mock(MOCK_ZIPFILE_NOLENGTH_URL, zipBuffer, {includeContentLength: false})
