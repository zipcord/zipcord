/** Feature detection flags  @packageDocumentation */


/* eslint-disable compat/compat */

/** Blob.stream() */
export const BLOB_STREAM = globalThis.Blob?.prototype.stream !== undefined
/** ReadableStream.pipeThrough() */
export const PIPE_THROUGH = globalThis.ReadableStream?.prototype.pipeThrough !== undefined
/** ReadableStream.pipeTo() */
export const PIPE_TO = globalThis.ReadableStream?.prototype.pipeTo !== undefined
/** Response.body */
const testResponse = globalThis.Response ? new Response() : null
export const RESPONSE_STREAM = testResponse?.body !== undefined
/** TransformStream */
export const TRANSFORM_STREAM = globalThis.TransformStream !== undefined
/** WritableStream */
export const WRITABLE_STREAM = globalThis.TransformStream !== undefined

/** ReadableStream.pipeThrough() and TransformStream */
export const PIPE_THROUGH_TRANSFORM = PIPE_THROUGH && TRANSFORM_STREAM
/** ReadableStream.pipeTo() and WritableStream */
export const PIPE_TO_WRITABLE = PIPE_TO && WRITABLE_STREAM

/* eslint-enable compat/compat */
