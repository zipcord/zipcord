/** Entry point for TypeDoc. Solves the double-export problem described in the
  link: https://github.com/TypeStrong/typedoc/issues/1197
*/

import { ZipcordRaw } from './zipcord-raw.js'


export * from './index-common.js'
export { ZipcordNode, nodeConfigSetup } from './index.js'
export { ZipcordFflate, fflateDecoderSetup } from './index-web.js'

/** Zipcord's main API.

  On opening an archive Zipcord will begin by downloading and parsing the
  footer structures of the file. The first thing it does is to read the end of
  the file for the EOCD and other records stored after the file entries. If you
  are reading from an URL source this means Zipcord will make at least one
  HTTP `Range` request when opening the file.

  If range requests are not supported by the server Zipcord will download the
  archive and treat it as a buffer source from then on. To prevent accidentally
  downloading excessively large files you can use the [[`rangeRequired`]] and
  [[ZipcordOptions.maxFallbackSize|`maxFallbackSize`]] options to control what
  happens in this case.

  Otherwise, if they are supported Zipcord will download a small amount of data
  from the end of the file to find the footers. This can be as little as 98
  bytes but by default will download more in case the archive has a comment
  (default of 1KB extra, configurable with [[`commentBufferSize`]]). If the
  archive has a very long comment that exceeds this buffer Zipcord will
  download the maximum size of the footers as a last resort (~64KB). It's also
  possible other variable length structures in the footers could trigger
  another request to fully download them, though most archives do not have
  comments and this is unlikely with default settings.

  Very small archives could end up being fully downloaded in these requests. In
  this case Zipcord will automatically switch to buffer reading and make no
  more HTTP requests.

  # Examples

  ```
  const archive = await Zipcord.open("http://example.com/archive.zip")
  // Print array of all filenames contained in the archive
  console.log(archive.filenames)

  // These will reject with a ZipReadError
  Zipcord.open("http://example.com/archive_1gb.zip", {maxFallbackSize: 1024**2})
  Zipcord.open("http://example.com/range_unsupported.zip", {rangeRequired: true})
  ```

  See [[`ZipEntry`]] for examples of how to read file entries from the archive.

  ---

  This is not an actual class defined by Zipcord but is an alias to
  [[`ZipcordFflate`]] on browser targets or to [[`ZipcordNode`]] when used in a
  Node script. Both of these classes extend [[`ZipcordRaw`]] to provide a
  default built-in decoder. You can import them directly if you want to
  override these defaults, though `ZipcordNode` will not be available in a
  browser.
*/
export class Zipcord extends ZipcordRaw {}
