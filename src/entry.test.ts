/** Tests for Zipcord file entries */

import Zipcord from './'
import { ZipInvalidError, ZipUninitializedError, ZipUnsupportedError } from './errors'
import { decodeBufferSlice } from './util'
import { ZipcordRaw } from './zipcord-raw'
import {
  arrayBufferSha1, getEntryOrFail, getFile,
  COMPRESSION_TEXT, HELLO_FILENAME, HELLO_TEXT, SMILE_FILENAME, SMILE_SHA1,
  LOCAL_DIRPATH, LOCAL_FILEPATH
} from './testdata/index'

jest.mock('./features')


test('Extract a file as a buffer', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))

  const extracted = await getEntryOrFail(zip, HELLO_FILENAME).buffer()
  const text = decodeBufferSlice(extracted)
  expect(text).toEqual(HELLO_TEXT)

  const image = await getEntryOrFail(zip, SMILE_FILENAME).buffer()
  const imageHash = arrayBufferSha1(image.buffer)
  expect(imageHash).toEqual(SMILE_SHA1)
})

// Test currently disabled. Due to some bug (probably a weird interaction
// between JSDOM's and node-fetch's implementation of Blobs) this cannot be
// currently run in a NodeJS Jest environment. Might be possible to reenable
// when JSDOM supports Blob.text()/.arrayBuffer()
// https://github.com/jsdom/jsdom/issues/2555
// eslint-disable-next-line jest/no-disabled-tests
test.skip('Extract a file as a Blob', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))

  const extracted = await getEntryOrFail(zip, HELLO_FILENAME).blob()
  const text = await extracted.text()
  expect(text).toEqual(HELLO_TEXT)

  const image = await getEntryOrFail(zip, SMILE_FILENAME).blob()
  const imageHash = arrayBufferSha1(await image.arrayBuffer())
  expect(imageHash).toEqual(SMILE_SHA1)
})

test('Extract a file as a string', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
})

test('Extract a stored file', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)
})

test('Inflate a deflated file', async () => {
  const zip = await Zipcord.open(getFile('deflate.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(COMPRESSION_TEXT)
})

test('Inflate a deflated file (internal streams disabled)', async () => {
  const zip = await Zipcord.open(getFile('deflate.zip'), {disableInternalStreams: true})
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(COMPRESSION_TEXT)
})

test('Extract entry written in streamed mode (data descriptors)', async () => {
  const zip = await Zipcord.open(getFile('all-stream.zip'))
  const text = await getEntryOrFail(zip, HELLO_FILENAME).text()
  expect(text).toEqual(HELLO_TEXT)

  const zipDeflate = await Zipcord.open(getFile('deflate-stream.zip'))
  const textDeflate = await getEntryOrFail(zipDeflate, HELLO_FILENAME).text()
  expect(textDeflate).toEqual(COMPRESSION_TEXT)
})

test('Entry comment', async () => {
  const zip = await Zipcord.open(getFile('archive_comment.zip'))
  expect(getEntryOrFail(zip, HELLO_FILENAME).comment).toEqual('entry comment')
})

test('Verify entry structure parsing', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))

  const entryHello = getEntryOrFail(zip, HELLO_FILENAME)
  await entryHello.loadLocalHeader()
  expect(entryHello.localHeader).toMatchSnapshot('Hello.txt localHeader')

  const entryFolder = getEntryOrFail(zip, 'images/')
  await entryFolder.loadLocalHeader()
  expect(entryFolder.localHeader).toMatchSnapshot('images/ localHeader')

  const entrySmile = getEntryOrFail(zip, 'images/smile.gif')
  await entrySmile.loadLocalHeader()
  expect(entrySmile.localHeader).toMatchSnapshot('images/smile.gif localHeader')
})

test('Unicode symbol in filename + text', async () => {
  const zip = await Zipcord.open(getFile('utf8_in_name.zip'))
  const text = await getEntryOrFail(zip, '€15.txt').text()
  expect(text).toEqual('€15\n')
})

test('Unicode vomit everywhere', async () => {
  const vomit = 'Iñtërnâtiônàlizætiøn☃💩'
  const zip = await Zipcord.open(getFile('pile_of_poo.zip'))
  expect(zip.comment).toEqual(vomit)
  const entry = getEntryOrFail(zip, vomit + '.txt')
  expect(entry.comment).toEqual(vomit)
  const text = await entry.text()
  expect(text).toEqual(vomit)
})

test('Windows archive (backslash separator)', async () => {
  // File was created on Windows with IZArc
  const zip = await Zipcord.open(getFile('slashes_and_izarc.zip'))
  const text = await getEntryOrFail(zip, 'test\\Hello.txt').text()
  // Text ends in Windows line ending
  expect(text).toEqual('Hello world\r\n')
})

test('Local encoding in file entry names', async () => {
  const zip = await Zipcord.open(getFile('local_encoding_in_name.zip'))
  expect(zip.entries.has(LOCAL_DIRPATH)).toBe(true)
  expect(zip.entries.has(LOCAL_FILEPATH)).toBe(true)
  const extracted = await getEntryOrFail(zip, LOCAL_FILEPATH).buffer()
  // Entry is a zero-length file
  expect(extracted.byteLength).toEqual(0)
})

test('Info-ZIP Unicode path extra field', async () => {
  // See APPNOTE.TXT section 4.6.9, field 0x7075
  const zip = await Zipcord.open(getFile('infozip_unicode_path.zip'))
  const text = await getEntryOrFail(zip, '€15.txt').text()
  expect(text).toEqual('€15\n')
})

test('Read archive containing only subfolders', async () => {
  const zip = await Zipcord.open(getFile('subfolder.zip'))
  expect(getEntryOrFail(zip, 'folder/').size).toEqual(0)
  expect(getEntryOrFail(zip, 'folder/subfolder/').size).toEqual(0)
})

test('Extract a zero-length entry (subfolder)', async () => {
  const zip = await Zipcord.open(getFile('subfolder.zip'))
  const extracted = await getEntryOrFail(zip, 'folder/').buffer()
  expect(extracted.byteLength).toEqual(0)
})

test('Extra field attributes in local header', async () => {
  const zip = await Zipcord.open(getFile('extra_attributes.zip'))
  const entry = getEntryOrFail(zip, HELLO_FILENAME)
  await entry.loadLocalHeader()

  const lhField5455 = entry.localHeader.extraField[0x5455]
  expect(lhField5455).toBeDefined()
  expect(lhField5455.byteLength).toEqual(9)
  const lhField7875 = entry.localHeader.extraField[0x7875]
  expect(lhField7875).toBeDefined()
  expect(lhField7875.byteLength).toEqual(11)
})

test('Failure to initialize decoder should throw on extract', async () => {
  const zip = await ZipcordRaw.open(
    getFile('deflate.zip'),
    { decoderSetup: () => Promise.reject('Setup failure') }
  )
  const entry = getEntryOrFail(zip, HELLO_FILENAME)
  await expect(zip.decoder).rejects.toThrowErrorMatchingSnapshot('zip.decoder')
  await expect(entry.buffer()).rejects.toThrowErrorMatchingSnapshot('entry.buffer')
})

test('Entry size mismatch should throw', async () => {
  const zip = await Zipcord.open(getFile('invalid/bad_decompressed_size.zip'))
  const entry = getEntryOrFail(zip, HELLO_FILENAME)
  await expect( entry.buffer() ).rejects.toThrowErrorMatchingSnapshot()
})

test('CRC-32 mismatch should throw if checkCRC32 set', async () => {
  const zip = await Zipcord.open(getFile('invalid/crc32.zip'), {checkCRC32: true})
  const entry = getEntryOrFail(zip, HELLO_FILENAME)
  await expect(entry.buffer()).rejects.toThrowErrorMatchingSnapshot()

  // By default Zipcord should not check CRC-32
  const zipIgnore = await Zipcord.open(getFile('invalid/crc32.zip'))
  await expect(getEntryOrFail(zipIgnore, HELLO_FILENAME).buffer())
    .resolves.not.toThrow(ZipInvalidError)

  // Valid archives should not throw
  const zipOk = await Zipcord.open(getFile('all.zip'), {checkCRC32: true})
  await expect(getEntryOrFail(zipOk, HELLO_FILENAME).buffer())
    .resolves.not.toThrow(ZipInvalidError)
})

test('Invalid/unknown compression method', async () => {
  const zip = await Zipcord.open(getFile('invalid/compression.zip'))
  await expect(() => getEntryOrFail(zip, HELLO_FILENAME).buffer())
    .rejects.toThrow(ZipUnsupportedError)
})

test('Extracting encrypted file should throw error', async () => {
  const zip = await Zipcord.open(getFile('invalid/encrypted.zip'))
  await expect(() => getEntryOrFail(zip, HELLO_FILENAME).buffer())
    .rejects.toThrow(ZipUnsupportedError)
})

test('Invalid offset in central directory', async () => {
  const zip = await Zipcord.open(getFile('invalid/bad_offset.zip'))
  await expect(() => getEntryOrFail(zip, HELLO_FILENAME).buffer())
    .rejects.toThrow(ZipInvalidError)
})

test('Does not allow accessing localHeader before loading it', async () => {
  const zip = await Zipcord.open(getFile('all.zip'))
  const zipEntry = getEntryOrFail(zip, HELLO_FILENAME)

  expect(() => zipEntry.localHeader).toThrow(ZipUninitializedError)
  await zipEntry.loadLocalHeader()
  expect(() => zipEntry.localHeader).not.toThrow(ZipUninitializedError)
})
