/** Common code for ZipEntry implementations

  @packageDocumentation
*/

import { crc32 } from './crc32.js'
import {
  ZipError, ZipInvalidError, ZipcordError,
  ZipUninitializedError, ZipUnsupportedError
} from './errors.js'
import * as features from './features.js'
import {
  ZIP_LOCAL_FILE_HEADER_MIN_SIZE, LITTLE_ENDIAN, CompressionMethod,
  dateFromMSDOS, extraFieldMappingFromBuffer, generalBitFlagsFromRaw
} from './structs.js'
import { decodeBufferSlice, pumpStreamIntoSink } from './util.js'

import type { ZipCDEntry, ZipLocalFileHeader } from './structs.js'
import type { ZipcordRaw } from './zipcord-raw.js'


/** Documentation for this class is found in entry.ts (Node implementation) so
  Typedoc will find it correctly */
export class ZipEntryBase {
  /** [[`ZipcordRaw`]] object that created this entry */
  public readonly archive: ZipcordRaw
  /** Central directory record for this entry */
  public readonly cdEntry: ZipCDEntry
  /** Last modified date of the archive entry */
  public readonly date: Date
  /** Filename of this entry. If an Infozip Unicode path field is present it
    will override the central directory entry filename. */
  public readonly filename: string
  /** unchecked property for localHeader */
  #localHeader?: ZipLocalFileHeader

  /** Constructor for zip file entries

    @param filename - Filename of this entry. If an Infozip Unicode path field
      is present it will override the central directory entry filename.
    @param archive - [[`ZipcordRaw`]] object that created this entry
    @param cdEntry - Central directory record for this entry

    @h idden - ZipEntry should only be created by [[`ZipcordRaw`]]
  */
  constructor (filename: string, archive: ZipcordRaw, cdEntry: ZipCDEntry, ) {
    // Declared and set explicitly for TypeDoc documentation
    this.archive = archive
    this.cdEntry = cdEntry
    this.filename = filename
    this.date = dateFromMSDOS(cdEntry.lastModDate, cdEntry.lastModTime)
  }

  /** Download and extract file entry as a `Uint8Array`.

    Extracts file entries compressed with Deflate and stored uncompressed.
    Other compression methods (such as the proprietary Deflate64) are not
    supported.

    Checks status of decoder and throws an error if it could not be initialized
    (e.g. could not load the module when using the WASM decoder).

    If the [[`checkCRC32`]] option is set, calculates the CRC32 of the output
    data and throws an error if it is incorrect.

    @throws [[`ZipInvalidError`]] when length or CRC32 of output data is
      incorrect, or if the compressed bitstream is corrupted
    @throws [[`ZipReadError`]] when a problem is encountered reading the archive:
      ** HTTP 4xx/5xx error or other issues with HTTP request
      ** [[`rangeRequired`]] is set but HTTP server does not support `Range` header
      ** [[ZipcordOptions.maxFallbackSize|`maxFallbackSize`]] is set, `Range`
        header is unsupported, and archive exceeds limit
      ** [[ZipcordOptions.maxFallbackSize|`maxFallbackSize`]] is set but
        server is not responding with `Content-Length` or `Content-Range` headers
      ** Archive or entry exceeds Zipcord's file size limits (8 PB)
    @throws [[`ZipUnsupportedError`]] when entry requires a feature not
      supported by Zipcord:
      ** Unknown or unsupported compression method such as Deflate64
      ** File entry encryption
      ** Unknown flags in general purpose flags bitfield

    @category Buffer
  */
  public async buffer(): Promise<Uint8Array> {
    await this.preextractSetup()

    let data: Uint8Array
    if (this.localHeader.compressionMethod === CompressionMethod.Deflate) {
      try {
        data = await this.inflateToBuffer()
      } catch (e) {
        if (e instanceof ZipError) {
          // If any error created by Zipcord itself rethrow as-is
          throw e
        } else {
          // Wrap string errors (e.g. from the WASM decoder) and non-Zipcord
          // errors in a ZipInvalidError
          throw new ZipInvalidError('corruptData', e)
        }
      }
    } else if (this.localHeader.compressionMethod === CompressionMethod.Store) {
      const buffer = await this.archive._rangeFetcher.readArrayBuffer(
        this.getDataOffset(), this.compressedSize)
      data = new Uint8Array(buffer)
    } else {
      throw new ZipUnsupportedError('compression', this.localHeader.compressionMethod)
    }

    // Error checking of data
    if (data.byteLength !== this.size) {
      throw new ZipInvalidError('length', [this.size, data.byteLength])
    }
    if (this.archive.options.checkCRC32) {
      const crc = crc32(new Uint8Array(data))
      if (crc !== this.crc32) {
        throw new ZipInvalidError('crc32', [this.crc32, crc])
      }
    }

    return data
  }

  /** Download and extract the file as a `Blob` (more specifically, a `File`).
    Wraps [[`buffer`]] to create the Blob, see its documentation for details
    and possible errors.

    Will fail if the `File` constructor is not present (i.e. NodeJS without a
    polyfill).

    @category Buffer
  */
  public async blob(): Promise<File> {
    const data = await this.buffer()
    return new File([data], this.filename, {lastModified: this.date.valueOf()})
  }

  /** Download and extract the file as a string. Decodes using `TextDecoder`
    with results of [[`buffer`]], see its documentation for details and
    possible errors.

    @param encoding - Text encoding passed to `TextDecoder`. Defaults to "utf-8".

    @category Buffer
  */
  public async text(encoding?: string): Promise<string> {
    const decoder = new TextDecoder(encoding)
    return decoder.decode(await this.buffer())
  }

  /** Download and extract the file as a streamed `Uint8Array` buffer.

    Provides decoded file entry as a `ReadableStream`. If `TransformStream` and
    `ReadableStream.pipeThrough` are available, they will be used to decode the
    compressed data. Otherwise file entry is downloaded and decoded before
    resulting data is returned as a stream.

    Unlike the buffer methods, the CRC32 and length is NOT checked for a
    stream, no matter if the [[`checkCRC32`]] option is set.

    @throws Same errors as [[`buffer`]] (except invalid CRC32/length errors),
      but will also throw [[`ZipUnsupportedError`]] if `ReadableStream` is
      unavailable

    @category Stream
  */
  public async bufferStream(): Promise<ReadableStream<Uint8Array>> {
    await this.preextractSetup()

    let stream: ReadableStream<Uint8Array>
    if (this.localHeader.compressionMethod === CompressionMethod.Deflate) {
      try {
        stream = await this.inflateToStream()
      } catch (e) {
        if (e instanceof ZipError) {
          // If any error created by Zipcord itself rethrow as-is
          throw e
        } else {
          // Wrap string errors (e.g. from the WASM decoder) and non-Zipcord
          // errors in a ZipInvalidError
          throw new ZipInvalidError('corruptData', e)
        }
      }
    } else if (this.localHeader.compressionMethod === CompressionMethod.Store) {
      stream = await this.getRawStream()
    } else {
      throw new ZipUnsupportedError('compression', this.localHeader.compressionMethod)
    }

    // Length/CRC32 is NOT checked for a stream

    return stream
  }

  /** Download and extract the file as streamed text. Decodes a stream from
    [[`bufferStream`]], see its documentation for details and possible errors.

    @param encoding - Text encoding passed to TextDecoder. Defaults to "utf-8".

    @category Stream
  */
  public async textStream(encoding?: string): Promise<ReadableStream<string>> {
    const decoder = new TextDecoder(encoding)
    const stream = await this.bufferStream()
    let outStream: ReadableStream<string>

    if (features.PIPE_THROUGH_TRANSFORM) {
      const transformer: Transformer<Uint8Array, string> = {
        transform(chunk, controller) {
          const outChunk = decoder.decode(chunk, {stream: true})
          controller.enqueue(outChunk)
        }
      }
      const decoderStream = new TransformStream(transformer)
      outStream = stream.pipeThrough(decoderStream)
    } else {
      // Manually pump a ReadableStream through the decoder
      const reader = stream.getReader()
      const source: UnderlyingSource<string> = {
        async start(controller) {
          while (controller) {
            const {value, done} = await reader.read()
            if (value) {
              const outChunk = decoder.decode(value.buffer, {stream: true})
              controller.enqueue(outChunk)
            }
            if (done) {
              controller.close()
              break
            }
          }
        }
      }
      outStream = new ReadableStream(source)
    }

    return outStream
  }

  /** Read and parse the local file header of the zip entry. Use if you want to
    access [[`localHeader`]] before extracting the file entry.
  */
  public async loadLocalHeader(): Promise<ZipLocalFileHeader> {
    // Use cached header if already downloaded and parsed
    if (this.#localHeader) {
      return this.#localHeader
    }

    // Filename/extra size may not match CD. Load a few bytes extra to avoid
    // making a second HTTP request
    const buffer = await this.archive._rangeFetcher.readArrayBuffer(
      this.getLocalHeaderOffset() + this.archive.offsetDelta,
      ZIP_LOCAL_FILE_HEADER_MIN_SIZE + this.cdEntry.filenameLength +
      this.cdEntry.extraFieldLength + this.archive.options.localHeaderExtraBufferSize)
    this.#localHeader = this.parseLocalHeader(buffer)

    return this.#localHeader
  }

  /** Read and parse the local file header of the zip entry */
  protected parseLocalHeader(buffer: ArrayBuffer): ZipLocalFileHeader {
    const view = new DataView(buffer)

    const version = view.getUint16(4, LITTLE_ENDIAN)
    const generalBitFlags = generalBitFlagsFromRaw(view.getUint16(6, LITTLE_ENDIAN))
    const compressionMethod: CompressionMethod = view.getUint16(8, LITTLE_ENDIAN)
    const filenameOffset = ZIP_LOCAL_FILE_HEADER_MIN_SIZE
    const filenameLength = view.getUint16(26, LITTLE_ENDIAN)
    const extraFieldOffset = ZIP_LOCAL_FILE_HEADER_MIN_SIZE + filenameLength
    const extraFieldLength = view.getUint16(28, LITTLE_ENDIAN)

    // Check buffer large enough
    if (
      (filenameLength + extraFieldLength) -
      (this.cdEntry.filenameLength + this.cdEntry.extraFieldLength) >
        this.archive.options.localHeaderExtraBufferSize
    ) {
      /* The extraField data is not always the same length in the local header
         as in the central directory. For example, the field 0x5455 stores UNIX
         timestamps for a file and is longer in the local header; Info-ZIP is a
         very popular tool that will create this field. See this link for
         details about this field's contents:
         https://commons.apache.org/proper/commons-compress/apidocs/org/apache/commons/compress/archivers/zip/X5455_ExtendedTimestamp.html

         The safety buffer will load a few extra bytes to
         cover this case, if you are getting this error you will need to
         configure a larger safety buffer. However, unless you are working with
         Zip encoders that are outputting very strange data in the extra fields
         you will probably never need to worry about this. */
      throw new ZipcordError('Safety buffer exceeded')
    }

    const extraField = extraFieldMappingFromBuffer(buffer, extraFieldOffset, extraFieldLength)
    const filename = decodeBufferSlice(buffer, filenameOffset, filenameLength)

    const localHeader = {
      signature: view.getUint32(0, LITTLE_ENDIAN),
      versionExtract: version,
      generalBitFlags,
      compressionMethod,
      lastModTime: view.getUint16(10, LITTLE_ENDIAN),
      lastModDate: view.getUint16(12, LITTLE_ENDIAN),
      crc32: view.getUint32(14, LITTLE_ENDIAN),
      compressedSize: view.getUint32(18, LITTLE_ENDIAN),
      uncompressedSize: view.getUint32(22, LITTLE_ENDIAN),
      filenameLength,
      extraFieldLength,
      filename,
      extraField,
    }

    return localHeader
  }

  /** Deflate a compressed file. Prefers to read from a `ReadableStream` if
    available but will retrieve compressed data as a buffer if not.
  */
  protected async inflateToBuffer(): Promise<Uint8Array> {
    const decoder = await this.archive.decoder
    let data: Promise<Uint8Array>
    if (this.archive.options.disableInternalStreams || !this.archive._rangeFetcher.streamingEnabled) {
      // If internal streams disabled or source does not allow reading from a
      // stream decode straight from a buffer to another buffer
      const buffer = await this.archive._rangeFetcher.readArrayBuffer(
        this.getDataOffset(), this.compressedSize
      )
      data = decoder.decodeBuffer(new Uint8Array(buffer), this.uncompressedSize)
    } else {
      // Else streams can be used in this context
      /** Decompressor */
      const [sink, dataFromSink] = decoder.createSink(this.uncompressedSize)
      data = dataFromSink
      const stream = await this.getRawStream()

      if (features.PIPE_TO_WRITABLE) {
        // If available use the more efficient pipeTo a WritableStream
        const deflateStream = new WritableStream(sink)
        await stream.pipeTo(deflateStream)
      } else {
        // Otherwise pump the stream manually
        await pumpStreamIntoSink(stream, sink)
      }
    }

    return data
  }

  /** Inflates a compressed file to a `ReadableStream`. If `TransformStream`/
    `pipeThrough` is available use them, otherwise download and decode the entry
    and stream the resulting buffer. */
  protected async inflateToStream(): Promise<ReadableStream<Uint8Array>> {
    let outStream: ReadableStream<Uint8Array>
    if (this.archive._rangeFetcher.streamingEnabled) {
      // ReadableStream is available

      if (features.PIPE_THROUGH_TRANSFORM) {
        const stream = await this.getRawStream()
        const transformer = (await this.archive.decoder).createTransformer(this.cdEntry.uncompressedSize)
        const decodeStream = new TransformStream(transformer)
        outStream = stream.pipeThrough(decodeStream)
      } else if (features.BLOB_STREAM) {
        // Can't pipe streams so decode as a buffer and stream that buffer instead
        const contents = await this.blob()
        outStream = contents.stream()
      } else {
        throw new ZipUnsupportedError('environment', 'TransformStream/pipeThrough or Blob.stream() not available')
      }
    } else {
      // ReadableStream is NOT available
      throw new ZipUnsupportedError('environment', 'ReadableStream not available')
    }

    return outStream
  }

  /** Offset of start of compressed data */
  protected getDataOffset(): number {
    return this.getLocalHeaderOffset() + ZIP_LOCAL_FILE_HEADER_MIN_SIZE +
      this.localHeader.filenameLength + this.localHeader.extraFieldLength +
      this.archive.offsetDelta
  }

  /** Offset of localHeader for this entry */
  protected getLocalHeaderOffset(): number {
    return (
      this.cdEntry.extraField.zip64?.localHeaderOffset ??
      this.cdEntry.localHeaderOffset
    )
  }

  /** Returns a `ReadableStream` with the raw file entry data */
  protected async getRawStream(): Promise<ReadableStream<Uint8Array>> {
    return this.archive._rangeFetcher.readStream(
      this.getDataOffset(), this.compressedSize
    )
  }

  /** Common extraction setup code for both buffer/streaming methods. Throws if
   the archive structure is corrupted or the entry is encrypted. */
  protected async preextractSetup(): Promise<void> {
    if (this.encrypted) {
      throw new ZipUnsupportedError('encryption')
    }
    // Check that read will not go past end of file (corrupted zip)
    if (
      (this.getLocalHeaderOffset() + ZIP_LOCAL_FILE_HEADER_MIN_SIZE +
       this.cdEntry.filenameLength) > this.archive.filesize
    ) {
      throw new ZipInvalidError(
        'corruptArchive', `Entry for ${this.filename} exceeds archive size`
      )
    }

    await this.loadLocalHeader()
  }

  // Accessors
  /////////////

  /** Size of compressed file */
  public get compressedSize(): number {
    // Use CD's value for compressedSize, as localHeader's value may be 0 if
    // bit 3 of flags is set (values stored after compressed data)
    return (
      this.#localHeader?.extraField.zip64?.compressedSize ??
      this.cdEntry.extraField.zip64?.compressedSize ??
      this.cdEntry.compressedSize
    )
  }

  /** Comment for this entry */
  public get comment(): string | undefined {
    if (this.cdEntry.commentLength > 0) {
      return this.cdEntry.comment
    } else {
      return
    }
  }

  /** CRC32 of uncompressed data */
  public get crc32(): number {
    return this.cdEntry.crc32
  }

  /** True if entry is encrypted. Encryted file entries are not supported by
    Zipcord and it will throw an error on trying to extract one.
  */
  public get encrypted(): boolean {
    return this.cdEntry.generalBitFlags.encrypted
  }

  /** Entry's zip localHeader structure */
  public get localHeader(): ZipLocalFileHeader {
    if (!this.#localHeader) {
      throw new ZipUninitializedError(
        'Must call loadLocalHeader or read entry contents before accessing localHeader'
      )
    }
    return this.#localHeader
  }

  /** Size of uncompressed file */
  public get size(): number {
    return (
      this.#localHeader?.extraField.zip64?.uncompressedSize ??
      this.cdEntry.extraField.zip64?.uncompressedSize ??
      this.cdEntry.uncompressedSize
    )
  }

  /** Size of file after decompression */
  public get uncompressedSize(): number {
    // Use CD's value for uncompressedSize, as localHeader's value may be 0 if
    // bit 3 of flags is set (values stored after compressed data)
    return (
      this.#localHeader?.extraField.zip64?.uncompressedSize ??
      this.cdEntry.extraField.zip64?.uncompressedSize ??
      this.cdEntry.uncompressedSize
    )
  }
}
