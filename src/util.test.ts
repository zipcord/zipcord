/** Tests for utility functions */

import { TextEncoder } from 'util'

import { decodeBufferSlice, sourceIsUrl } from './util'


test('decodeBufferSlice()', () => {
  const encoder = new TextEncoder()
  const buffer = encoder.encode('helloworld')
  const emptyBuffer = new ArrayBuffer(0)

  expect(decodeBufferSlice(buffer, 0, 5)).toEqual('hello')
  expect(decodeBufferSlice(buffer, 5, 5)).toEqual('world')
  expect(decodeBufferSlice(emptyBuffer, 0, 0)).toEqual('')

  // Optional arguments
  expect(decodeBufferSlice(buffer, 0)).toEqual('helloworld')
  expect(decodeBufferSlice(buffer, 5)).toEqual('world')
  expect(decodeBufferSlice(buffer)).toEqual('helloworld')
})

// Implement test when I figure out ReadableStream testing
test.todo('pumpStreamIntoSink()')

test('sourceIsUrl() type guard', () => {
  expect(sourceIsUrl('URL source')).toBeTruthy()
  expect(sourceIsUrl(new ArrayBuffer(0))).toBeFalsy()
})
