/* NOTE: Unfortunately ts-node still does not play will with ESM imports. This
script has been written in JS instead of TS because I got tired of fighting
ts-node over a <100 line script. In the future when ts-node has better ESM
support this could possibly be ported to TS.
*/

import fs from 'fs'

import AbortController from 'node-abort-controller'
import fetch from 'node-fetch'

import Zipcord, { CompressionMethod } from '../dist/index.js'


// Polyfills for URL reading
globalThis.AbortController = AbortController
globalThis.fetch = fetch


function dumpArchive(archive, filename) {
  console.log(`Info dump for ${filename}`)
  if (archive.eocd.commentLength > 0) {
    console.log(`Comment: ${archive.eocd.comment}`)
  }
  console.log(`Total entries: ${archive.eocd.entriesTotal} (${archive.eocd.entriesOnDisk} this disk #${archive.eocd.diskNum})\n`)

  console.log('Legend: <filename> - <compressed size> => <uncompressed size> (<compression method>), <flags>')
  for (const record of archive.centralDirectory) {
    const size = `${record.compressedSize} => ${record.uncompressedSize}`
    let method
    switch (record.compressionMethod) {
      case CompressionMethod.Deflate:
        method = 'Deflate'
        break
      case CompressionMethod.Store:
        method = 'Store'
        break
      default:
        method = `Unsupported compression method ${record.compressionMethod}`
    }
    const flags = '' +
      (record.generalBitFlags.encrypted ? 'e' : '') +
      (record.generalBitFlags.strongEncryption ? 'E' : '')
    const flagsString = flags.length > 0 ? ', ' + flags : flags

    console.log(`${record.filename} - ${size} (${method})${flagsString}`)
  }
}

function dumpEntry(entry) {
  console.log(`Entry for ${entry.filename}`)
  if (entry.comment && entry.comment.length > 0) {
    console.log('Comment: ' + entry.comment)
  }
  const deflatedSize = entry.localHeader.compressionMethod !== CompressionMethod.Store ?
    ` (compressed to ${entry.compressedSize})` :
    ' (stored uncompressed)'
  console.log(`\tSize: ${entry.uncompressedSize}` + deflatedSize)
  console.log(`\tCRC32: ${entry.crc32.toString(16)}`)
  console.log(`\tDate: ${entry.date}`)
}

async function main() {
  if (process.argv.length !== 3) {
    console.error('Must provide path or URL of a valid zip archive as only argument')
    return
  }

  const archivePath = process.argv[2]
  let zip
  if (archivePath.startsWith('http')) {
    zip = await Zipcord.open(archivePath)
  } else {
    try {
      const buffer = fs.readFileSync(archivePath).buffer
      zip = await Zipcord.open(buffer)
    } catch (e) {
      console.error('Could not open archive: ' + e)
      return
    }
  }

  dumpArchive(zip, archivePath)
  console.log(/* This space intentionally left blank */)
  for (const entry of zip.entries.values()) {
    await entry.loadLocalHeader()
    dumpEntry(entry)
  }
}
main()
