export default {
  preset: 'ts-jest',
  globals: {
    'ts-jest': {
      diagnostics: {
        // Disable unnecessary warning about modules
        // https://github.com/kulshekhar/ts-jest/issues/748#issuecomment-423528659
        ignoreCodes: [151001]
      }
    }
  },
  moduleFileExtensions: ['js', 'ts'],
  moduleNameMapper: {
    /* Imports in the .ts source files are written using a .js file extension,
      so Node can import the built ESM modules without needing to set
      --experimental-specifier-resolution=node. However this breaks ts-jest
      because it doesn't understand these imports (see
      https://github.com/TypeStrong/ts-node/issues/783 for a discussion on
      this.) so we need to translate them back into TS-style extensionless
      imports. */
    [`^\\./(.*).js$`]: './$1',
  },
  setupFiles: ['./src/testdata/setupTests.ts'],
  transform: {
    "^.+\\.ts$": "ts-jest",
    "^.+\\.js$": "babel-jest",
  },
}
