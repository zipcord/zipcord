/* In this project the ESLint configuration is in a JS module referenced from
package.json's eslintConfig. This is done because JSON is a horrible format for
configuration files and this method allows for comments and sane syntax. In a
perfect world JSON configuration files are sharing a room in Hell with YAML. */

module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    /** Allow ESLint to check configuration files in the project root */
    extraFileExtensions: ['.cjs'],
    project: './tsconfig.noexclude.json'
  },
  extends: [
    'eslint:recommended',
    'plugin:eslint-comments/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:compat/recommended',
    'plugin:jest/recommended',
    'plugin:jest/style'
  ],
  env: {
    browser: true,
    node: true
  },
  ignorePatterns: ['**/*.d.ts'],
  settings: {
    // Expected to be polyfilled and ignored by the compat plugin
    polyfills: ['fetch', 'TextDecoder']
  },
  rules: {
    'eqeqeq': 'error',
    'key-spacing': 'error',
    // Allow console.log for debugging because I am lazy and bad at debuggers
    'no-console': 'warn',
    'no-multiple-empty-lines': 'error',
    'no-trailing-spaces': 'error',
    'no-unneeded-ternary': 'error',
    'no-unsafe-optional-chaining': 'error',
    'prefer-const': 'warn',
    // I will die on this hill
    'semi': [
      'error',
      'never',
      {
        beforeStatementContinuationChars: 'always'
      }
    ],
    '@typescript-eslint/consistent-type-imports': 'error',
    '@typescript-eslint/explicit-function-return-type': [
      'warn',
      {
        allowExpressions: true
      }
    ],
    '@typescript-eslint/member-delimiter-style': [
      'warn',
      {
        multiline: {
          delimiter: 'none'
        }
      }
    ],
    '@typescript-eslint/naming-convention': [
      'warn',
      { selector: 'memberLike', format: ['camelCase'], leadingUnderscore: 'allow' },
      { selector: 'enumMember', format: ['PascalCase'] },
      { selector: 'property', format: null, leadingUnderscore: 'allow',
        filter: { regex: '[- ]|Range', match: true } },
      { selector: 'typeLike', format: ['PascalCase'] },
      { selector: 'variableLike', format: ['camelCase'] },
      { selector: 'variable', format: ['camelCase', 'UPPER_CASE'] },
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow' },
    ],
    '@typescript-eslint/no-empty-function': 'off',
  }
}
